# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v2.4.0 - 2024-09-05(09:29:49 +0000)

## Release v2.3.1 - 2024-07-24(13:07:05 +0000)

## Release v2.3.0 - 2023-01-30(14:34:55 +0000)

### New

- - [libqosmodule] Integrate library

## Release v2.2.0 - 2023-01-24(15:20:44 +0000)

### Changes

- [tr181-qos] random: QoS.Queue reported as misconfigured

## Release v2.1.2 - 2022-11-17(11:22:34 +0000)

### Fixes

- - [mod-qos-tc] Error_Misconfigured Status when AssuredRate is unconfigured

## Release v2.1.1 - 2022-11-08(09:47:14 +0000)

## Release v2.1.0 - 2022-09-01(10:15:06 +0000)

### New

- - [prpl][qos] Integrate new TR-181 parameters for Device.QoS v2.16

## Release v2.0.1 - 2022-08-24(08:34:29 +0000)

### Other

- - [mod-qos-tc] Runtime dependency on libqosnode is missing

## Release v2.0.0 - 2022-08-03(11:32:14 +0000)

## Release v1.0.5 - 2022-05-19(12:59:00 +0000)

### Fixes

- [Gitlab CI][Unit tests][valgrind] Pipeline doesn't stop when memory leaks are detected

## Release v1.0.4 - 2022-05-18(10:37:13 +0000)

### Other

- Correct dependencies

## Release v1.0.3 - 2022-05-02(09:36:53 +0000)

### Fixes

- - [mod-qos-tc] HTB shaping rate is set incorrectly

## Release v1.0.2 - 2022-02-26(07:23:40 +0000)

### Fixes

- - [prpl][qos][tc] Invalid free of variants

## Release v1.0.1 - 2022-02-21(08:22:07 +0000)

### Fixes

- Module does not compile for Openwrt

### Other

- Correct gitlab CI dependencies

