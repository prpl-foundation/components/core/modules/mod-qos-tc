/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>

#include <amxc/amxc_macros.h>
#include <amxc/amxc.h>
#include <amxm/amxm.h>
#include <debug/sahtrace.h>
#include "tc_qos.h"
#include "configure_tc.h"
#include "configure_tbf.h"
#include "configure_prio.h"
#include "configure_htb.h"
#include "configure_dropalgo.h"
#include "configure_filter.h"
#include <netlink/route/tc.h>
#include <netlink/route/qdisc.h>
#include <netlink/route/qdisc/htb.h>
#include <netlink/route/qdisc/red.h>
#include <netlink/route/cls/fw.h>
#include <qosnode/qos-node-api.h>
#include <qosmodule/api.h>

int qos_queue_activate(UNUSED const char* function_name,
                       amxc_var_t* args,
                       UNUSED amxc_var_t* ret) {
    qos_node_t* queue = qos_node_get_node(amxc_var_constcast(cstring_t, args));
    qos_node_t* queue_parent = qos_node_find_parent_by_type(queue, QOS_NODE_TYPE_QUEUE);
    int retval = -2;
    int shaping_rate;
    uint32_t qid, qparent_id, queue_key, limit;
    qos_scheduler_algorithm_t scheduler_algo;
    qos_queue_drop_algorithm_t drop_algo;
    const char* ifname = NULL;

    when_null(queue, exit);
    when_false(queue->type == QOS_NODE_TYPE_QUEUE, exit);

    shaping_rate = qos_node_queue_get_shaping_rate(queue);
    scheduler_algo = qos_node_queue_get_scheduler_algorithm(queue);
    qid = qos_node_queue_get_index(queue);
    ifname = qos_node_queue_get_interface(queue);
    qparent_id = queue_parent ? (qos_node_queue_get_index(queue_parent)) : 0;
    queue_key = qos_node_queue_get_queue_key(queue);

    //the limit is set to 3 times the minimum threshold since the max threshold is not specified in tr181
    limit = qos_node_queue_get_red_threshold(queue) * 3;
    drop_algo = qos_node_queue_get_drop_algorithm(queue);

    when_str_empty(ifname, exit);
    when_true((qid == 0), exit);
    when_true(scheduler_algo == QOS_SCHEDULER_ALGORITHM_LAST, exit);

    if(scheduler_algo == QOS_SCHEDULER_ALGORITHM_TBF) {
        qid = 0; // attach drop algorithm to root qdisc
    } else if((scheduler_algo == QOS_SCHEDULER_ALGORITHM_SP) && (shaping_rate > 0)) {
        retval = configure_tbf_shaper(qid, ifname,
                                      qos_node_queue_get_buffer_length(queue),
                                      shaping_rate / 8, // bps -> Bps
                                      qos_node_queue_get_shaping_burst_size(queue));
        when_failed(retval, exit);
        qid += SHAPER_OFFSET; // attach drop algorithm to tbf qdisc
    } else if(scheduler_algo == QOS_SCHEDULER_ALGORITHM_HTB) {
        if(shaping_rate <= 0) {
            SAH_TRACEZ_ERROR(ME, "HTB queue must have a shaping rate! cannot configure queue %d", qid);
            goto exit;
        }
        retval = activate_htb_queue(queue, qparent_id);
        when_failed(retval, exit);
    }

    if((drop_algo != QOS_QUEUE_DROP_ALGORITHM_DT) && (drop_algo != QOS_QUEUE_DROP_ALGORITHM_LAST)) {
        retval = configure_drop_algorithm(qid, ifname, drop_algo, limit);
        when_failed(retval, exit);
    }
    if(queue_key != 0) {
        retval = configure_filter(queue);
    }

exit:
    return retval;
}

int qos_queue_deactivate(UNUSED const char* function_name,
                         amxc_var_t* args,
                         UNUSED amxc_var_t* ret) {

    qos_node_t* queue = qos_node_get_node(amxc_var_constcast(cstring_t, args));
    qos_node_t* queue_parent = qos_node_find_parent_by_type(queue, QOS_NODE_TYPE_QUEUE);
    int retval = -2, error;
    int shaping_rate;
    qos_scheduler_algorithm_t scheduler_algo;
    qos_queue_drop_algorithm_t drop_algo;
    uint32_t qid, qparent_id, drop_qid_offset = 0;
    const char* ifname = NULL;
    netlink_return netlink_data;

    when_null(queue, exit);
    when_false(queue->type == QOS_NODE_TYPE_QUEUE, exit);

    shaping_rate = qos_node_queue_get_shaping_rate(queue);
    scheduler_algo = qos_node_queue_get_scheduler_algorithm(queue);
    drop_algo = qos_node_queue_get_drop_algorithm(queue);
    qid = qos_node_queue_get_index(queue);
    if(scheduler_algo == QOS_SCHEDULER_ALGORITHM_TBF) {
        drop_qid_offset = (uint32_t) -qid; // drop qdisc attached to root
    } else if((scheduler_algo == QOS_SCHEDULER_ALGORITHM_SP) && (shaping_rate > 0)) {
        drop_qid_offset = SHAPER_OFFSET;   // drop qdisc attached to shaper
    }
    ifname = qos_node_queue_get_interface(queue);
    qparent_id = queue_parent ? (qos_node_queue_get_index(queue_parent)) : 0;

    when_str_empty(ifname, exit);

    error = cleanup_filter(queue);
    if(error != 0) {
        SAH_TRACEZ_INFO(ME, "cleanup of filter for queue: %d on interface: %s returned: %d", qid, ifname, error);
    }
    if((drop_algo != QOS_QUEUE_DROP_ALGORITHM_DT) && (drop_algo != QOS_QUEUE_DROP_ALGORITHM_LAST)) {
        error = cleanup_drop_algorithm(qid + drop_qid_offset, ifname);
        if(error != 0) {
            SAH_TRACEZ_INFO(ME, "cleanup of dropalgo for queue: %d on interface: %s returned: %d", qid, ifname, error);
        }
    }
    if(scheduler_algo == QOS_SCHEDULER_ALGORITHM_TBF) {
        when_true_status(true, exit, retval = 0); // tbf has no classes
    }
    if(scheduler_algo == QOS_SCHEDULER_ALGORITHM_SP) {
        if(shaping_rate > 0) {
            error = cleanup_tbf_shaper(qid, ifname);
            if(error != 0) {
                SAH_TRACEZ_INFO(ME, "cleanup of tbf shaper for queue: %d on interface: %s returned: %d", qid, ifname, error);
            }
        }
        when_true_status(true, exit, retval = 0); // prio classes are created&destroyed automatically
    }

    retval = connect_to_netlink(&netlink_data, RTNL_TC_TYPE_CLASS, ifname);
    when_failed(retval, exit);

    rtnl_tc_set_link(TC_CAST(netlink_data.traffic_class), netlink_data.link);

    if(qparent_id == 0) {
        qos_node_t* tmp, * tmp2, * scheduler, * shaper;

        //Todo: find a safer way to get shaperid
        tmp = queue;
        while((tmp2 = qos_node_find_parent_by_type(tmp, QOS_NODE_TYPE_QUEUE)) != NULL) {
            tmp = tmp2;
        }

        shaper = qos_node_find_parent_by_type(tmp, QOS_NODE_TYPE_SHAPER);
        if(shaper == NULL) {
            scheduler = qos_node_find_parent_by_type(tmp, QOS_NODE_TYPE_SCHEDULER);
            if(scheduler) {
                shaper = qos_node_find_parent_by_type(scheduler, QOS_NODE_TYPE_SHAPER);
            }
        }

        rtnl_tc_set_parent(TC_CAST(netlink_data.traffic_class), TC_HANDLE(1, SHAPER_OFFSET + qos_node_shaper_get_index(shaper)));
    } else {
        rtnl_tc_set_parent(TC_CAST(netlink_data.traffic_class), TC_HANDLE(1, qparent_id));
    }

    rtnl_tc_set_handle(TC_CAST(netlink_data.traffic_class), TC_HANDLE(1, qid));
    retval = rtnl_class_delete(netlink_data.sock, netlink_data.traffic_class);

    cleanup_netlink(&netlink_data, RTNL_TC_TYPE_CLASS);

exit:
    return retval;
}

int qos_shaper_activate(UNUSED const char* function_name,
                        amxc_var_t* args,
                        UNUSED amxc_var_t* ret) {

    qos_node_t* shaper = qos_node_get_node(amxc_var_constcast(cstring_t, args));
    qos_node_t* scheduler = qos_node_find_parent_by_type(shaper, QOS_NODE_TYPE_SCHEDULER);
    int retval = -2;
    qos_scheduler_algorithm_t sched_algo;

    when_null(shaper, exit);

    if(!scheduler) {
        scheduler = qos_node_find_child_by_type(shaper, QOS_NODE_TYPE_SCHEDULER);
    }

    when_null(scheduler, exit);

    sched_algo = qos_node_scheduler_get_scheduler_algorithm(scheduler);
    when_true_status(sched_algo == QOS_SCHEDULER_ALGORITHM_TBF || sched_algo == QOS_SCHEDULER_ALGORITHM_SP, exit, retval = 0);

    if(sched_algo == QOS_SCHEDULER_ALGORITHM_HTB) {
        retval = activate_htb_shaper(shaper);
    } else {
        SAH_TRACEZ_ERROR(ME, "Scheduler algorithm %s not supported", qos_scheduler_algorithm_to_string(sched_algo));
        retval = -2;
    }

exit:
    return retval;
}

int qos_shaper_deactivate(UNUSED const char* function_name,
                          amxc_var_t* args,
                          UNUSED amxc_var_t* ret) {
    qos_node_t* shaper = qos_node_get_node(amxc_var_constcast(cstring_t, args));
    qos_node_t* scheduler = qos_node_find_parent_by_type(shaper, QOS_NODE_TYPE_SCHEDULER);
    int retval = -2;
    qos_scheduler_algorithm_t sched_algo;
    const char* ifname = qos_node_shaper_get_interface(shaper);

    when_null(shaper, exit);
    when_str_empty(ifname, exit);

    if(!scheduler) {
        scheduler = qos_node_find_child_by_type(shaper, QOS_NODE_TYPE_SCHEDULER);
    }
    when_null(scheduler, exit);

    sched_algo = qos_node_scheduler_get_scheduler_algorithm(scheduler);
    when_true_status(sched_algo == QOS_SCHEDULER_ALGORITHM_TBF || sched_algo == QOS_SCHEDULER_ALGORITHM_SP, exit, retval = 0);

    netlink_return netlink_data;
    retval = connect_to_netlink(&netlink_data, RTNL_TC_TYPE_CLASS, ifname);
    when_failed(retval, exit);

    rtnl_tc_set_link(TC_CAST(netlink_data.traffic_class), netlink_data.link);
    rtnl_tc_set_parent(TC_CAST(netlink_data.traffic_class), TC_HANDLE(1, 0));
    rtnl_tc_set_handle(TC_CAST(netlink_data.traffic_class), TC_HANDLE(1, SHAPER_OFFSET + qos_node_shaper_get_index(shaper)));

    retval = rtnl_class_delete(netlink_data.sock, netlink_data.traffic_class);
    cleanup_netlink(&netlink_data, RTNL_TC_TYPE_CLASS);

exit:
    return retval;
}

int qos_scheduler_activate(UNUSED const char* function_name,
                           amxc_var_t* args,
                           UNUSED amxc_var_t* ret) {
    qos_node_t* scheduler = qos_node_get_node(amxc_var_constcast(cstring_t, args));
    qos_node_t* default_queue = qos_node_scheduler_get_default_queue_node(scheduler);
    int retval = -2;
    qos_scheduler_algorithm_t sched_algo = qos_node_scheduler_get_scheduler_algorithm(scheduler);
    const char* ifname = qos_node_scheduler_get_interface(scheduler);

    when_null(scheduler, exit);
    when_str_empty(ifname, exit);

    if(sched_algo == QOS_SCHEDULER_ALGORITHM_TBF) {
        amxc_llist_t* queues;
        qos_node_t* queue;
        int shaping_rate;

        // TBF scheduler supports only one queue
        queue = qos_node_find_child_by_type(scheduler, QOS_NODE_TYPE_QUEUE);
        if(queue) {
            queues = qos_node_get_children(scheduler);
        } else {
            qos_node_t* shaper = qos_node_find_child_by_type(scheduler, QOS_NODE_TYPE_SHAPER);

            queue = shaper ? qos_node_find_child_by_type(shaper, QOS_NODE_TYPE_QUEUE) : NULL;
            if(queue) {
                queues = qos_node_get_children(shaper);
            } else {
                queues = NULL;
            }
        }
        when_true(queues == NULL || queue == NULL || amxc_llist_size(queues) != 1, exit);

        shaping_rate = qos_node_queue_get_shaping_rate(queue);
        when_true(shaping_rate <= 0, exit);

        retval = configure_tbf_shaper(0, ifname,
                                      qos_node_queue_get_buffer_length(queue),
                                      shaping_rate / 8, // bps -> Bps
                                      qos_node_queue_get_shaping_burst_size(queue));
        when_failed(retval, exit);
    } else if(sched_algo == QOS_SCHEDULER_ALGORITHM_SP) {
        uint8_t bands = 0;
        uint8_t* priomap = NULL;
        uint8_t priomap_count = 0;
        amxc_llist_t* queues = NULL;

        // Retrieve the number of bands (i.e. count the queues)
        if(qos_node_find_child_by_type(scheduler, QOS_NODE_TYPE_QUEUE)) {
            queues = qos_node_get_children(scheduler);
        } else {
            qos_node_t* shaper = qos_node_find_child_by_type(scheduler, QOS_NODE_TYPE_SHAPER);
            queues = shaper ? qos_node_get_children(shaper) : NULL;
        }
        if(queues) {
            bands = amxc_llist_size(queues);
        }

        // tc custo enabled by QoS.BrokenQDiscPrioMap flag:
        //   priomap is a list of consecutive queue IDs from 0 to #bands - 1
        if(bands && qos_node_get_broken_qdisc_priomap()) {
            unsigned i;

            priomap = alloca(bands);
            priomap_count = bands;
            for(i = 0; i < bands; i++) {
                priomap[i] = i;
            }
        }

        retval = activate_prio_scheduler(ifname, bands, priomap, priomap_count);
    } else if(sched_algo == QOS_SCHEDULER_ALGORITHM_HTB) {
        retval = activate_htb_scheduler(scheduler, qos_node_queue_get_index(default_queue));
    } else {
        SAH_TRACEZ_ERROR(ME, "Scheduler algorithm %s not supported", qos_scheduler_algorithm_to_string(sched_algo));
    }

exit:
    return retval;
}

int qos_scheduler_deactivate(UNUSED const char* function_name,
                             amxc_var_t* args,
                             UNUSED amxc_var_t* ret) {
    qos_node_t* scheduler = qos_node_get_node(amxc_var_constcast(cstring_t, args));
    int retval = -2;
    const char* ifname = qos_node_scheduler_get_interface(scheduler);
    netlink_return netlink_data;

    when_str_empty(ifname, exit);

    retval = connect_to_netlink(&netlink_data, RTNL_TC_TYPE_QDISC, ifname);
    when_failed(retval, exit);

    rtnl_tc_set_link(TC_CAST(netlink_data.qdisc), netlink_data.link);
    rtnl_tc_set_parent(TC_CAST(netlink_data.qdisc), TC_H_ROOT);
    rtnl_tc_set_handle(TC_CAST(netlink_data.qdisc), TC_HANDLE(1, 0));

    retval = rtnl_qdisc_delete(netlink_data.sock, netlink_data.qdisc);
    cleanup_netlink(&netlink_data, RTNL_TC_TYPE_QDISC);

exit:
    return retval;
}

int qos_stats_retrieve(UNUSED const char* function_name,
                       amxc_var_t* args,
                       amxc_var_t* ret) {
    qos_node_t* queue = qos_node_get_node(amxc_var_constcast(cstring_t, args));
    int retval = -2;
    const char* ifname = qos_node_queue_get_interface(queue);
    uint32_t qid = qos_node_queue_get_index(queue);
    int ifindex = 0;
    uint64_t outputpackets = 0;
    uint64_t outputbytes = 0;
    uint64_t droppedpackets = 0;
    netlink_return netlink_data;
    struct nl_cache* cache = NULL;
    struct rtnl_class* tc_class = NULL;

    when_str_empty(ifname, exit);

    retval = -1;
    when_null(ret, exit);

    retval = connect_to_netlink(&netlink_data, RTNL_TC_TYPE_CLASS, ifname);
    when_failed(retval, exit);

    ifindex = rtnl_link_get_ifindex(netlink_data.link);

    retval = rtnl_class_alloc_cache(netlink_data.sock, ifindex, &cache);
    when_failed(retval, exit);

    tc_class = rtnl_class_get(cache, ifindex, TC_HANDLE(1, qid));
    when_null(tc_class, exit);

    outputpackets = rtnl_tc_get_stat(TC_CAST(tc_class), RTNL_TC_PACKETS);
    outputbytes = rtnl_tc_get_stat(TC_CAST(tc_class), RTNL_TC_BYTES);
    droppedpackets = rtnl_tc_get_stat(TC_CAST(tc_class), RTNL_TC_DROPS);

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint64_t, ret, "TxPackets", outputpackets);
    amxc_var_add_key(uint64_t, ret, "TxBytes", outputbytes);
    amxc_var_add_key(uint64_t, ret, "DroppedPackets", droppedpackets);

    cleanup_netlink(&netlink_data, RTNL_TC_TYPE_CLASS);

exit:
    nl_cache_put(cache);
    rtnl_class_put(tc_class);
    return retval;
}

int qos_queue_stats_retrieve(UNUSED const char* function_name,
                             amxc_var_t* args,
                             amxc_var_t* ret) {
    qos_node_t* queue = qos_node_get_node(amxc_var_constcast(cstring_t, args));
    int retval = -2;
    const char* ifname = qos_node_queue_get_interface(queue);
    uint32_t qid = qos_node_queue_get_index(queue);
    int ifindex = 0;
    uint64_t currentrate = 0;
    netlink_return netlink_data;
    struct nl_cache* cache = NULL;
    struct rtnl_class* tc_class = NULL;

    when_str_empty(ifname, exit);

    retval = -1;
    when_null(ret, exit);

    retval = connect_to_netlink(&netlink_data, RTNL_TC_TYPE_CLASS, ifname);
    when_failed(retval, exit);

    ifindex = rtnl_link_get_ifindex(netlink_data.link);

    retval = rtnl_class_alloc_cache(netlink_data.sock, ifindex, &cache);
    when_failed(retval, exit);

    tc_class = rtnl_class_get(cache, ifindex, TC_HANDLE(1, qid));
    when_null(tc_class, exit);

    currentrate = rtnl_tc_get_stat(TC_CAST(tc_class), RTNL_TC_RATE_BPS);

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint64_t, ret, "CurrentShapingRate", currentrate);
    // Todo: change when a better way to get assuredrate is found
    amxc_var_add_key(uint64_t, ret, "CurrentAssuredRate", qos_node_queue_get_assured_rate(queue));

    cleanup_netlink(&netlink_data, RTNL_TC_TYPE_CLASS);

exit:
    nl_cache_put(cache);
    rtnl_class_put(tc_class);
    return retval;
}

static amxm_module_t* mod = NULL;

/**
 * @ingroup mod-qos-tc
 * @brief
 * Constructor function when the module is loaded by tr181-qos.
 *
 * @returns 0 on success, otherwise -1.
 */

#ifndef UNIT_TEST
static AMXM_CONSTRUCTOR
#else
int
#endif
mod_tc_start(void) {

    qos_module_api_funcs_t funcs = {
        .activate_shaper = qos_shaper_activate,
        .activate_scheduler = qos_scheduler_activate,
        .activate_queue = qos_queue_activate,
        .deactivate_shaper = qos_shaper_deactivate,
        .deactivate_scheduler = qos_scheduler_deactivate,
        .deactivate_queue = qos_queue_deactivate,
        .retrieve_statistics = qos_stats_retrieve,
        .retrieve_queue_statistics = qos_queue_stats_retrieve,
    };

    SAH_TRACEZ_INFO(ME, "start");
    int retval = qos_module_register(&mod, &funcs);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to register module!");
        retval = -1;
    }

    return retval;

}

/**
 * @ingroup mod-qos-tc
 * @brief
 * Destructor function when the module is unloaded by tr181-qos.
 *
 * @returns 0, even when errors occur.
 */

#ifndef UNIT_TEST
static AMXM_DESTRUCTOR
#else
int
#endif
mod_tc_stop(void) {
    int retval = -1;
    retval = qos_module_deregister(&mod);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to deregister module!");
    }
    return 0;
}
