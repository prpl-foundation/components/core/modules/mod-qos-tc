/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>

#include <amxc/amxc_macros.h>
#include <debug/sahtrace.h>
#include "tc_qos.h"
#include "configure_tc.h"
#include "configure_htb.h"
#include <netlink/route/tc.h>
#include <netlink/route/qdisc.h>
#include <netlink/route/qdisc/htb.h>
#include <netlink/route/classifier.h>
#include <netlink/route/class.h>
#include <qosnode/qos-node-api.h>

int activate_htb_scheduler(qos_node_t* scheduler, uint32_t defaultqueue) {
    int retval = -2;
    netlink_return netlink_data;
    const char* ifname = qos_node_scheduler_get_interface(scheduler);

    when_str_empty(ifname, exit);

    retval = connect_to_netlink(&netlink_data, RTNL_TC_TYPE_QDISC, ifname);
    when_failed(retval, exit);

    rtnl_tc_set_link(TC_CAST(netlink_data.qdisc), netlink_data.link);
    rtnl_tc_set_parent(TC_CAST(netlink_data.qdisc), TC_H_ROOT);
    rtnl_tc_set_handle(TC_CAST(netlink_data.qdisc), TC_HANDLE(1, 0));
    when_failed(rtnl_tc_set_kind(TC_CAST(netlink_data.qdisc), "htb"), failed);
    if(defaultqueue != 0) {
        when_failed(rtnl_htb_set_defcls(netlink_data.qdisc, TC_HANDLE(1, defaultqueue)), misconfigured);
    }

    retval = rtnl_qdisc_add(netlink_data.sock, netlink_data.qdisc, NLM_F_CREATE);
    cleanup_netlink(&netlink_data, RTNL_TC_TYPE_QDISC);

exit:
    return retval;

failed:
    cleanup_netlink(&netlink_data, RTNL_TC_TYPE_QDISC);
    retval = -1;
    return retval;

misconfigured:
    cleanup_netlink(&netlink_data, RTNL_TC_TYPE_QDISC);
    retval = -2;
    return retval;
}

int activate_htb_shaper(qos_node_t* shaper) {
    int retval = -2;
    netlink_return netlink_data;
    const char* ifname = qos_node_shaper_get_interface(shaper);
    uint32_t rate = qos_node_shaper_get_shaping_rate(shaper) / 8;
    uint32_t burst_size = qos_node_shaper_get_shaping_burst_size(shaper);

    when_str_empty(ifname, exit);

    retval = connect_to_netlink(&netlink_data, RTNL_TC_TYPE_CLASS, ifname);
    when_failed(retval, exit);

    rtnl_tc_set_link(TC_CAST(netlink_data.traffic_class), netlink_data.link);
    rtnl_tc_set_parent(TC_CAST(netlink_data.traffic_class), TC_HANDLE(1, 0));
    rtnl_tc_set_handle(TC_CAST(netlink_data.traffic_class), TC_HANDLE(1, SHAPER_OFFSET + qos_node_shaper_get_index(shaper)));
    when_failed(rtnl_tc_set_kind(TC_CAST(netlink_data.traffic_class), "htb"), failed);
    when_failed(rtnl_htb_set_rate(netlink_data.traffic_class, rate), misconfigured);
    if(burst_size != 0) {
        when_failed(rtnl_htb_set_cbuffer(netlink_data.traffic_class, burst_size), misconfigured);
    }

    retval = rtnl_class_add(netlink_data.sock, netlink_data.traffic_class, NLM_F_CREATE);
    cleanup_netlink(&netlink_data, RTNL_TC_TYPE_CLASS);

exit:
    return retval;

failed:
    cleanup_netlink(&netlink_data, RTNL_TC_TYPE_CLASS);
    retval = -1;
    return retval;

misconfigured:
    cleanup_netlink(&netlink_data, RTNL_TC_TYPE_CLASS);
    retval = -2;
    return retval;
}

int activate_htb_queue(qos_node_t* queue, uint32_t parentid) {
    int retval = -2;
    netlink_return netlink_data;
    const char* ifname = qos_node_queue_get_interface(queue);
    uint32_t qid = qos_node_queue_get_index(queue);
    int32_t max_rate = qos_node_queue_get_shaping_rate(queue);
    int32_t min_rate = qos_node_queue_get_assured_rate(queue);
    int32_t rate = 0;
    int32_t ceil = 0;
    uint32_t precedence = qos_node_queue_get_precedence(queue);
    uint32_t burst_size = qos_node_queue_get_shaping_burst_size(queue);
    qos_node_t* shaper;

    when_str_empty(ifname, exit);
    when_false(qid, exit);
    when_true(max_rate == -1, misconfigured); //For HTB, rate is mandatory.
    max_rate /= 8;                            //From bps to Bps.
    min_rate /= 8;                            //From bps to Bps.
    when_true(max_rate == 0, misconfigured);  //Rate was configured too low.

    if(min_rate > 0) {
        when_true(min_rate > max_rate, misconfigured);
        rate = min_rate;
        ceil = max_rate;
    } else {
        rate = ceil = max_rate;
    }

    retval = connect_to_netlink(&netlink_data, RTNL_TC_TYPE_CLASS, ifname);
    when_failed(retval, exit);
    rtnl_tc_set_link(TC_CAST(netlink_data.traffic_class), netlink_data.link);
    if(parentid == 0) {
        shaper = qos_node_find_parent_by_type(queue, QOS_NODE_TYPE_SHAPER);
        rtnl_tc_set_parent(TC_CAST(netlink_data.traffic_class), TC_HANDLE(1, SHAPER_OFFSET + qos_node_shaper_get_index(shaper)));
    } else {
        rtnl_tc_set_parent(TC_CAST(netlink_data.traffic_class), TC_HANDLE(1, parentid));
    }
    rtnl_tc_set_handle(TC_CAST(netlink_data.traffic_class), TC_HANDLE(1, qid));
    when_failed(rtnl_tc_set_kind(TC_CAST(netlink_data.traffic_class), "htb"), failed);


    when_failed(rtnl_htb_set_rate(netlink_data.traffic_class, rate), failed);
    when_failed(rtnl_htb_set_ceil(netlink_data.traffic_class, ceil), failed);

    when_failed(rtnl_htb_set_prio(netlink_data.traffic_class, precedence), failed);

    if(burst_size != 0) {
        when_failed(rtnl_htb_set_cbuffer(netlink_data.traffic_class, burst_size), failed);
    }

    retval = rtnl_class_add(netlink_data.sock, netlink_data.traffic_class, NLM_F_CREATE);
    cleanup_netlink(&netlink_data, RTNL_TC_TYPE_CLASS);

exit:
    return retval;

failed:
    cleanup_netlink(&netlink_data, RTNL_TC_TYPE_CLASS);
    retval = -1;
    return retval;

misconfigured:
    retval = -2;
    return retval;
}

