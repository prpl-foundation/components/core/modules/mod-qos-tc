/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>

#include <amxc/amxc_macros.h>
#include <debug/sahtrace.h>
#include <netlink/route/tc.h>
#include <netlink/route/cls/fw.h>
#include <linux/if_ether.h>
#include "configure_filter.h"
#include "configure_tc.h"

int configure_filter(qos_node_t* queue) {
    int retval = -2;
    const char* ifname = qos_node_queue_get_interface(queue);
    uint32_t qid = qos_node_queue_get_index(queue);
    uint32_t queuekey = qos_node_queue_get_queue_key(queue);
    uint32_t prio = qos_node_queue_get_precedence(queue);
    netlink_return netlink_data;

    when_str_empty(ifname, exit);
    when_false(qid, exit);

    retval = connect_to_netlink(&netlink_data, RTNL_TC_TYPE_CLS, ifname);
    when_failed(retval, exit);
    rtnl_tc_set_link(TC_CAST(netlink_data.cls), netlink_data.link);
    rtnl_tc_set_handle(TC_CAST(netlink_data.cls), queuekey);
    rtnl_tc_set_parent(TC_CAST(netlink_data.cls), TC_HANDLE(1, 0));
    when_failed(rtnl_tc_set_kind(TC_CAST(netlink_data.cls), "fw"), failed);

    rtnl_cls_set_protocol(netlink_data.cls, ETH_P_ALL);
    rtnl_cls_set_prio(netlink_data.cls, prio);
    when_failed(rtnl_fw_set_classid(netlink_data.cls, TC_HANDLE(1, qid)), failed);
    when_failed(rtnl_fw_set_mask(netlink_data.cls, qos_node_get_mark_mask()), misconfigured);
    retval = rtnl_cls_add(netlink_data.sock, netlink_data.cls, NLM_F_CREATE);
    cleanup_netlink(&netlink_data, RTNL_TC_TYPE_CLS);

exit:
    return retval;

failed:
    cleanup_netlink(&netlink_data, RTNL_TC_TYPE_CLS);
    return -1;

misconfigured:
    cleanup_netlink(&netlink_data, RTNL_TC_TYPE_CLS);
    return -2;
}

int cleanup_filter(qos_node_t* queue) {
    int retval = -2;
    const char* ifname = qos_node_queue_get_interface(queue);
    uint32_t qid = qos_node_queue_get_index(queue);
    uint32_t queuekey = qos_node_queue_get_queue_key(queue);
    uint32_t prio = qos_node_queue_get_precedence(queue);
    netlink_return netlink_data;

    when_str_empty(ifname, exit);
    when_false(qid, exit);

    retval = connect_to_netlink(&netlink_data, RTNL_TC_TYPE_CLS, ifname);
    when_failed(retval, exit);
    rtnl_tc_set_link(TC_CAST(netlink_data.cls), netlink_data.link);
    rtnl_tc_set_handle(TC_CAST(netlink_data.cls), queuekey);
    rtnl_tc_set_parent(TC_CAST(netlink_data.cls), TC_HANDLE(1, 0));
    when_failed(rtnl_tc_set_kind(TC_CAST(netlink_data.cls), "fw"), failed);
    rtnl_cls_set_prio(netlink_data.cls, prio);
    when_failed(rtnl_fw_set_classid(netlink_data.cls, TC_HANDLE(1, qid)), failed);
    retval = rtnl_cls_delete(netlink_data.sock, netlink_data.cls, NLM_F_REQUEST);

    cleanup_netlink(&netlink_data, RTNL_TC_TYPE_CLS);

exit:
    return retval;

failed:
    cleanup_netlink(&netlink_data, RTNL_TC_TYPE_CLS);
    return -1;
}

