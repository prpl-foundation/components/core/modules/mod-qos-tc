/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>

#include <amxc/amxc_macros.h>
#include <debug/sahtrace.h>
#include "tc_qos.h"
#include "configure_tc.h"
#include <netlink/route/tc.h>
#include <netlink/route/qdisc.h>
#include <netlink/route/qdisc/htb.h>
#include <netlink/route/classifier.h>
#include <netlink/route/cls/fw.h>
#include <netlink/route/class.h>

void cleanup_netlink(netlink_return* ret, enum rtnl_tc_type type) {
    if(ret == NULL) {
        return;
    }

    if(type == RTNL_TC_TYPE_QDISC) {
        rtnl_qdisc_put(ret->qdisc);
    } else {
        if(type == RTNL_TC_TYPE_CLASS) {
            rtnl_class_put(ret->traffic_class);
        } else {
            rtnl_cls_put(ret->cls);
        }
    }
    nl_cache_put(ret->cache);
    rtnl_link_put(ret->link);
    nl_socket_free(ret->sock);
}

int connect_to_netlink(netlink_return* ret, enum rtnl_tc_type type, const char* ifname) {
    int retval = -1;

    if(ret == NULL) {
        return retval;
    }

    when_str_empty(ifname, misconfigured);

    ret->qdisc = NULL;
    ret->traffic_class = NULL;
    ret->cls = NULL;
    ret->sock = NULL;
    ret->cache = NULL;
    ret->link = NULL;

    ret->sock = nl_socket_alloc();
    if(ret->sock == NULL) {
        return retval;
    }
    retval = nl_connect(ret->sock, NETLINK_ROUTE);
    if(retval != 0) {
        cleanup_netlink(ret, type);
        return retval;
    }
    retval = rtnl_link_alloc_cache(ret->sock, AF_UNSPEC, &(ret->cache));
    if(retval < 0) {
        cleanup_netlink(ret, type);
        return retval;
    }
    if(!(ret->link = rtnl_link_get_by_name(ret->cache, ifname))) {
        retval = -1;
        cleanup_netlink(ret, type);
        return retval;
    }
    if(type == RTNL_TC_TYPE_QDISC) {
        if(!(ret->qdisc = rtnl_qdisc_alloc())) {
            cleanup_netlink(ret, type);
            retval = -1;
            return retval;
        }
    } else {
        if(type == RTNL_TC_TYPE_CLASS) {
            if(!(ret->traffic_class = rtnl_class_alloc())) {
                retval = -1;
                cleanup_netlink(ret, type);
                return retval;
            }
        } else {
            if(!(ret->cls = rtnl_cls_alloc())) {
                cleanup_netlink(ret, type);
                retval = -1;
                return retval;
            }
        }
    }
    return retval;

misconfigured:
    return -2;
}
