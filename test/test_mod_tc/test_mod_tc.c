/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <unistd.h>
#include <string.h>

#include <amxc/amxc_llist.h>
#include <amxc/amxc_macros.h>
#include "tc_qos.h"
#include "configure_tc.h"
#include "configure_filter.h"
#include "configure_htb.h"
#include "configure_prio.h"
#include "configure_tbf.h"
#include "configure_dropalgo.h"

#include "test_mod_tc.h"

void test_connect_to_netlink(UNUSED void** state) {
    int retval = -1;
    netlink_return netlink_data;
    const char* ifname = "eth0";

    retval = connect_to_netlink(NULL, RTNL_TC_TYPE_QDISC, ifname);
    assert_int_equal(retval, -1);
    cleanup_netlink(NULL, RTNL_TC_TYPE_QDISC);

    retval = connect_to_netlink(&netlink_data, RTNL_TC_TYPE_QDISC, NULL);
    assert_int_equal(retval, -2);

    will_return(__wrap_nl_socket_alloc, -1);

    retval = connect_to_netlink(&netlink_data, RTNL_TC_TYPE_QDISC, ifname);
    assert_int_equal(retval, -1);

    will_return_always(__wrap_nl_socket_alloc, 0);
    will_return(__wrap_nl_connect, -1);

    retval = connect_to_netlink(&netlink_data, RTNL_TC_TYPE_QDISC, ifname);
    assert_int_equal(retval, -1);

    will_return_always(__wrap_nl_connect, 0);
    will_return(__wrap_rtnl_link_alloc_cache, -1);

    retval = connect_to_netlink(&netlink_data, RTNL_TC_TYPE_QDISC, ifname);
    assert_int_equal(retval, -1);

    will_return_always(__wrap_rtnl_link_alloc_cache, 0);

    will_return(__wrap_rtnl_link_get_by_name, -1);
    retval = connect_to_netlink(&netlink_data, RTNL_TC_TYPE_QDISC, ifname);
    assert_int_equal(retval, -1);

    will_return_always(__wrap_rtnl_link_get_by_name, 0);

    will_return(__wrap_rtnl_qdisc_alloc, -1);

    retval = connect_to_netlink(&netlink_data, RTNL_TC_TYPE_QDISC, ifname);
    assert_int_equal(retval, -1);

    will_return_always(__wrap_rtnl_qdisc_alloc, 0);

    retval = connect_to_netlink(&netlink_data, RTNL_TC_TYPE_QDISC, ifname);
    assert_int_equal(retval, 0);

    cleanup_netlink(&netlink_data, RTNL_TC_TYPE_QDISC);

    will_return(__wrap_rtnl_class_alloc, -1);

    retval = connect_to_netlink(&netlink_data, RTNL_TC_TYPE_CLASS, ifname);
    assert_int_equal(retval, -1);

    will_return_always(__wrap_rtnl_class_alloc, 0);

    retval = connect_to_netlink(&netlink_data, RTNL_TC_TYPE_CLASS, ifname);
    assert_int_equal(retval, 0);

    cleanup_netlink(&netlink_data, RTNL_TC_TYPE_CLASS);

    will_return(__wrap_rtnl_cls_alloc, -1);

    retval = connect_to_netlink(&netlink_data, RTNL_TC_TYPE_CLS, ifname);
    assert_int_equal(retval, -1);

    will_return_always(__wrap_rtnl_cls_alloc, 0);

    retval = connect_to_netlink(&netlink_data, RTNL_TC_TYPE_CLS, ifname);
    assert_int_equal(retval, 0);

    cleanup_netlink(&netlink_data, RTNL_TC_TYPE_CLS);

}

void test_configure_cleanup_filters(UNUSED void** state) {
    int retval = -1;
    qos_node_t* queue = NULL;
    qos_node_t* invalid_queue = NULL;
    int error = -1;
    int misconfigured = -2;

    //set mocks ok so connect to netlink will be successful
    will_return_always(__wrap_nl_socket_alloc, 0);
    will_return_always(__wrap_nl_connect, 0);
    will_return_always(__wrap_rtnl_link_get_by_name, 0);
    will_return_always(__wrap_rtnl_link_alloc_cache, 0);
    will_return_always(__wrap_rtnl_cls_alloc, 0);
    will_return_always(__wrap_qos_node_get_mark_mask, 31);

    will_return(__wrap_rtnl_tc_set_kind, -1);
    will_return(__wrap_qos_node_queue_get_interface, "eth0");
    will_return(__wrap_qos_node_queue_get_index, 1);
    will_return(__wrap_qos_node_queue_get_queue_key, 2051);
    will_return(__wrap_qos_node_queue_get_precedence, 5);
    retval = configure_filter(queue);
    assert_int_equal(retval, error);

    will_return_always(__wrap_rtnl_tc_set_kind, 0);
    will_return(__wrap_rtnl_fw_set_classid, -1);
    will_return(__wrap_qos_node_queue_get_interface, "eth0");
    will_return(__wrap_qos_node_queue_get_index, 1);
    will_return(__wrap_qos_node_queue_get_queue_key, 2051);
    will_return(__wrap_qos_node_queue_get_precedence, 5);
    retval = configure_filter(queue);
    assert_int_equal(retval, error);

    will_return(__wrap_rtnl_fw_set_classid, -1);
    will_return(__wrap_qos_node_queue_get_interface, "eth0");
    will_return(__wrap_qos_node_queue_get_index, 1);
    will_return(__wrap_qos_node_queue_get_queue_key, 2051);
    will_return(__wrap_qos_node_queue_get_precedence, 5);
    retval = cleanup_filter(queue);
    assert_int_equal(retval, error);

    will_return_always(__wrap_rtnl_fw_set_classid, 0);
    will_return(__wrap_rtnl_fw_set_mask, -1);
    will_return(__wrap_qos_node_queue_get_interface, "eth0");
    will_return(__wrap_qos_node_queue_get_index, 1);
    will_return(__wrap_qos_node_queue_get_queue_key, 2051);
    will_return(__wrap_qos_node_queue_get_precedence, 5);
    retval = configure_filter(queue);
    assert_int_equal(retval, misconfigured);

    will_return_always(__wrap_rtnl_fw_set_mask, 0);

    will_return(__wrap_qos_node_queue_get_interface, NULL);
    will_return(__wrap_qos_node_queue_get_index, 0);
    will_return(__wrap_qos_node_queue_get_queue_key, 0);
    will_return(__wrap_qos_node_queue_get_precedence, 0);
    retval = configure_filter(invalid_queue);
    assert_int_equal(retval, misconfigured);

    will_return(__wrap_qos_node_queue_get_interface, "eth0");
    will_return(__wrap_qos_node_queue_get_index, 1);
    will_return(__wrap_qos_node_queue_get_queue_key, 2051);
    will_return(__wrap_qos_node_queue_get_precedence, 5);
    retval = configure_filter(queue);
    assert_int_equal(retval, 0);

    will_return(__wrap_qos_node_queue_get_interface, NULL);
    will_return(__wrap_qos_node_queue_get_index, 0);
    will_return(__wrap_qos_node_queue_get_queue_key, 0);
    will_return(__wrap_qos_node_queue_get_precedence, 0);
    retval = cleanup_filter(invalid_queue);
    assert_int_equal(retval, misconfigured);

    will_return(__wrap_qos_node_queue_get_interface, "eth0");
    will_return(__wrap_qos_node_queue_get_index, 1);
    will_return(__wrap_qos_node_queue_get_queue_key, 2051);
    will_return(__wrap_qos_node_queue_get_precedence, 5);
    retval = cleanup_filter(queue);
    assert_int_equal(retval, 0);
}

void test_activate_htb_scheduler(UNUSED void** state) {
    int retval = -1;
    qos_node_t* scheduler = NULL;
    qos_node_t* invalid_scheduler = NULL;
    uint32_t defaultqueue = 1;
    int error = -1;
    int misconfigured = -2;

    //set mocks for connect to netlink
    will_return_always(__wrap_nl_socket_alloc, 0);
    will_return_always(__wrap_nl_connect, 0);
    will_return_always(__wrap_rtnl_link_get_by_name, 0);
    will_return_always(__wrap_rtnl_link_alloc_cache, 0);
    will_return_always(__wrap_rtnl_qdisc_alloc, 0);

    will_return(__wrap_rtnl_tc_set_kind, -1);
    will_return(__wrap_qos_node_scheduler_get_interface, "eth0");
    retval = activate_htb_scheduler(scheduler, defaultqueue);
    assert_int_equal(retval, error);

    will_return_always(__wrap_rtnl_tc_set_kind, 0);
    will_return(__wrap_rtnl_htb_set_defcls, -1);
    will_return(__wrap_qos_node_scheduler_get_interface, "eth0");
    retval = activate_htb_scheduler(scheduler, defaultqueue);
    assert_int_equal(retval, misconfigured);

    will_return(__wrap_rtnl_qdisc_add, -1);
    will_return_always(__wrap_rtnl_htb_set_defcls, 0);
    will_return(__wrap_qos_node_scheduler_get_interface, "eth0");
    retval = activate_htb_scheduler(scheduler, defaultqueue);
    assert_int_equal(retval, error);

    will_return_always(__wrap_rtnl_qdisc_add, 0);

    will_return(__wrap_qos_node_scheduler_get_interface, NULL);
    retval = activate_htb_scheduler(invalid_scheduler, defaultqueue);
    assert_int_equal(retval, -2);

    will_return(__wrap_qos_node_scheduler_get_interface, "eth0");
    retval = activate_htb_scheduler(scheduler, defaultqueue);
    assert_int_equal(retval, 0);
}

void test_activate_htb_shaper(UNUSED void** state) {
    int retval = -1;
    int error = -1;
    int misconfigured = -2;
    qos_node_t* shaper = NULL;
    qos_node_t* invalid_shaper = NULL;

    //set mocks for connect to netlink
    will_return_always(__wrap_nl_socket_alloc, 0);
    will_return_always(__wrap_nl_connect, 0);
    will_return_always(__wrap_rtnl_link_get_by_name, 0);
    will_return_always(__wrap_rtnl_link_alloc_cache, 0);
    will_return_always(__wrap_rtnl_class_alloc, 0);

    will_return(__wrap_rtnl_tc_set_kind, -1);
    will_return(__wrap_qos_node_shaper_get_interface, "eth0");
    will_return(__wrap_qos_node_shaper_get_shaping_rate, 50000);
    will_return(__wrap_qos_node_shaper_get_shaping_burst_size, 30000);
    will_return(__wrap_qos_node_shaper_get_index, 1);
    retval = activate_htb_shaper(shaper);
    assert_int_equal(retval, error);

    will_return(__wrap_rtnl_tc_set_kind, 0);
    expect_any(__wrap_rtnl_htb_set_rate, class);
    expect_any(__wrap_rtnl_htb_set_rate, rate);
    will_return(__wrap_rtnl_htb_set_rate, -1);
    will_return(__wrap_qos_node_shaper_get_interface, "eth0");
    will_return(__wrap_qos_node_shaper_get_shaping_rate, 50000);
    will_return(__wrap_qos_node_shaper_get_shaping_burst_size, 30000);
    will_return(__wrap_qos_node_shaper_get_index, 1);
    retval = activate_htb_shaper(shaper);
    assert_int_equal(retval, misconfigured);

    expect_any(__wrap_rtnl_htb_set_rate, class);
    expect_any(__wrap_rtnl_htb_set_rate, rate);
    will_return(__wrap_rtnl_htb_set_rate, 0);
    will_return(__wrap_rtnl_tc_set_kind, 0);
    will_return(__wrap_rtnl_htb_set_cbuffer, 0);
    will_return(__wrap_rtnl_class_add, 0);
    will_return(__wrap_qos_node_shaper_get_interface, "eth0");
    will_return(__wrap_qos_node_shaper_get_shaping_rate, 50000);
    will_return(__wrap_qos_node_shaper_get_shaping_burst_size, 30000);
    will_return(__wrap_qos_node_shaper_get_index, 1);
    retval = activate_htb_shaper(shaper);
    assert_int_equal(retval, 0);

    expect_any(__wrap_rtnl_htb_set_rate, class);
    expect_any(__wrap_rtnl_htb_set_rate, rate);
    will_return(__wrap_rtnl_htb_set_rate, 0);
    will_return(__wrap_rtnl_tc_set_kind, 0);
    will_return(__wrap_rtnl_htb_set_cbuffer, -1);
    will_return(__wrap_qos_node_shaper_get_interface, "eth0");
    will_return(__wrap_qos_node_shaper_get_shaping_rate, 50000);
    will_return(__wrap_qos_node_shaper_get_shaping_burst_size, 30000);
    will_return(__wrap_qos_node_shaper_get_index, 1);
    retval = activate_htb_shaper(shaper);
    assert_int_equal(retval, misconfigured);

    expect_any(__wrap_rtnl_htb_set_rate, class);
    expect_any(__wrap_rtnl_htb_set_rate, rate);
    will_return(__wrap_rtnl_htb_set_rate, 0);
    will_return(__wrap_rtnl_tc_set_kind, 0);
    will_return(__wrap_rtnl_htb_set_cbuffer, 0);
    will_return(__wrap_rtnl_class_add, -1);
    will_return(__wrap_qos_node_shaper_get_interface, "eth0");
    will_return(__wrap_qos_node_shaper_get_shaping_rate, 50000);
    will_return(__wrap_qos_node_shaper_get_shaping_burst_size, 30000);
    will_return(__wrap_qos_node_shaper_get_index, 1);
    retval = activate_htb_shaper(shaper);
    assert_int_equal(retval, error);

    will_return(__wrap_qos_node_shaper_get_interface, NULL);
    will_return(__wrap_qos_node_shaper_get_shaping_rate, 0);
    will_return(__wrap_qos_node_shaper_get_shaping_burst_size, 0);
    retval = activate_htb_shaper(invalid_shaper);
    assert_int_equal(retval, -2);

    expect_any(__wrap_rtnl_htb_set_rate, class);
    expect_any(__wrap_rtnl_htb_set_rate, rate);
    will_return(__wrap_rtnl_htb_set_rate, 0);
    will_return(__wrap_rtnl_tc_set_kind, 0);
    will_return(__wrap_rtnl_htb_set_cbuffer, 0);
    will_return(__wrap_rtnl_class_add, 0);
    will_return(__wrap_qos_node_shaper_get_interface, "eth0");
    will_return(__wrap_qos_node_shaper_get_shaping_rate, 50000);
    will_return(__wrap_qos_node_shaper_get_shaping_burst_size, 30000);
    will_return(__wrap_qos_node_shaper_get_index, 1);
    retval = activate_htb_shaper(shaper);
    assert_int_equal(retval, 0);
}

void test_activate_htb_queue(UNUSED void** state) {
    int retval = -1;
    int error = -1;
    qos_node_t* queue = NULL;
    qos_node_t* invalid_queue = NULL;
    uint32_t parentid = 10;
    uint32_t no_parentid = 0;
    int misconfigured = -2;

    will_return_always(__wrap_nl_socket_alloc, 0);
    will_return_always(__wrap_nl_connect, 0);
    will_return_always(__wrap_rtnl_link_get_by_name, 0);
    will_return_always(__wrap_rtnl_link_alloc_cache, 0);
    will_return_always(__wrap_rtnl_class_alloc, 0);
    will_return_always(__wrap_qos_node_find_parent_by_type, NULL);

    //ShapingRate must not be -1.
    will_return(__wrap_qos_node_queue_get_interface, "eth0");
    will_return(__wrap_qos_node_queue_get_index, 1);
    will_return(__wrap_qos_node_queue_get_shaping_rate, -1);
    will_return(__wrap_qos_node_queue_get_assured_rate, -1);
    will_return(__wrap_qos_node_queue_get_precedence, 5);
    will_return(__wrap_qos_node_queue_get_shaping_burst_size, 3000);
    retval = activate_htb_queue(queue, parentid);
    assert_int_equal(retval, misconfigured);

    //ShapingRate cannot be too small (<8).
    will_return(__wrap_qos_node_queue_get_interface, "eth0");
    will_return(__wrap_qos_node_queue_get_index, 1);
    will_return(__wrap_qos_node_queue_get_shaping_rate, 7);
    will_return(__wrap_qos_node_queue_get_assured_rate, -1);
    will_return(__wrap_qos_node_queue_get_precedence, 5);
    will_return(__wrap_qos_node_queue_get_shaping_burst_size, 3000);
    retval = activate_htb_queue(queue, parentid);
    assert_int_equal(retval, misconfigured);

    //rtnl_tc_set_kind fails.
    will_return(__wrap_qos_node_queue_get_interface, "eth0");
    will_return(__wrap_qos_node_queue_get_index, 1);
    will_return(__wrap_qos_node_queue_get_shaping_rate, 50000);
    will_return(__wrap_qos_node_queue_get_assured_rate, -1);
    will_return(__wrap_qos_node_queue_get_precedence, 5);
    will_return(__wrap_qos_node_queue_get_shaping_burst_size, 3000);
    will_return(__wrap_rtnl_tc_set_kind, -1);
    retval = activate_htb_queue(queue, parentid);
    assert_int_equal(retval, error);

    //rtnl_htb_set_rate fails.
    will_return(__wrap_qos_node_queue_get_interface, "eth0");
    will_return(__wrap_qos_node_queue_get_index, 1);
    will_return(__wrap_qos_node_queue_get_shaping_rate, 50000);
    will_return(__wrap_qos_node_queue_get_assured_rate, -1);
    will_return(__wrap_qos_node_queue_get_precedence, 5);
    will_return(__wrap_qos_node_queue_get_shaping_burst_size, 3000);
    will_return_always(__wrap_rtnl_tc_set_kind, 0);
    expect_any(__wrap_rtnl_htb_set_rate, class);
    expect_any(__wrap_rtnl_htb_set_rate, rate);
    will_return(__wrap_rtnl_htb_set_rate, -1);
    retval = activate_htb_queue(queue, parentid);
    assert_int_equal(retval, error);

    //rntl_htb_set_prio fails.
    will_return(__wrap_qos_node_queue_get_interface, "eth0");
    will_return(__wrap_qos_node_queue_get_index, 1);
    will_return(__wrap_qos_node_queue_get_shaping_rate, 50000);
    will_return(__wrap_qos_node_queue_get_assured_rate, -1);
    will_return(__wrap_qos_node_queue_get_precedence, 5);
    will_return(__wrap_qos_node_queue_get_shaping_burst_size, 3000);
    expect_any(__wrap_rtnl_htb_set_rate, class);
    expect_any(__wrap_rtnl_htb_set_rate, rate);
    will_return_always(__wrap_rtnl_htb_set_rate, 0);
    expect_any(__wrap_rtnl_htb_set_ceil, class);
    expect_any(__wrap_rtnl_htb_set_ceil, ceil);
    will_return_always(__wrap_rtnl_htb_set_ceil, 0);
    will_return(__wrap_rtnl_htb_set_prio, -1);
    retval = activate_htb_queue(queue, parentid);
    assert_int_equal(retval, error);

    //rtnl_htb_set_cbuffer fails.
    will_return(__wrap_qos_node_queue_get_interface, "eth0");
    will_return(__wrap_qos_node_queue_get_index, 1);
    will_return(__wrap_qos_node_queue_get_shaping_rate, 50000);
    will_return(__wrap_qos_node_queue_get_assured_rate, -1);
    will_return(__wrap_qos_node_queue_get_precedence, 5);
    will_return(__wrap_qos_node_queue_get_shaping_burst_size, 3000);
    will_return_always(__wrap_rtnl_htb_set_prio, 0);
    will_return(__wrap_rtnl_htb_set_cbuffer, -1);
    expect_any(__wrap_rtnl_htb_set_rate, class);
    expect_any(__wrap_rtnl_htb_set_rate, rate);
    expect_any(__wrap_rtnl_htb_set_ceil, class);
    expect_any(__wrap_rtnl_htb_set_ceil, ceil);
    retval = activate_htb_queue(queue, parentid);
    assert_int_equal(retval, error);

    //rtnl_class_add fails.
    will_return(__wrap_qos_node_queue_get_interface, "eth0");
    will_return(__wrap_qos_node_queue_get_index, 1);
    will_return(__wrap_qos_node_queue_get_shaping_rate, 50000);
    will_return(__wrap_qos_node_queue_get_assured_rate, -1);
    will_return(__wrap_qos_node_queue_get_precedence, 5);
    will_return(__wrap_qos_node_queue_get_shaping_burst_size, 3000);
    will_return_always(__wrap_rtnl_htb_set_cbuffer, 0);
    will_return(__wrap_rtnl_class_add, -1);
    expect_any(__wrap_rtnl_htb_set_rate, class);
    expect_any(__wrap_rtnl_htb_set_rate, rate);
    expect_any(__wrap_rtnl_htb_set_ceil, class);
    expect_any(__wrap_rtnl_htb_set_ceil, ceil);
    retval = activate_htb_queue(queue, parentid);
    assert_int_equal(retval, error);

    will_return_always(__wrap_rtnl_class_add, 0);

    //ShapingRate is set to 125 Mbps, no AssuredRate is set.
    will_return(__wrap_qos_node_queue_get_interface, "eth0");
    will_return(__wrap_qos_node_queue_get_index, 1);
    will_return(__wrap_qos_node_queue_get_shaping_rate, 125000000);
    will_return(__wrap_qos_node_queue_get_assured_rate, -1);
    will_return(__wrap_qos_node_queue_get_precedence, 1);
    will_return(__wrap_qos_node_queue_get_shaping_burst_size, 1500);
    expect_any(__wrap_rtnl_htb_set_rate, class);
    expect_value(__wrap_rtnl_htb_set_rate, rate, 15625000); //ShapingRate / 8.
    expect_any(__wrap_rtnl_htb_set_ceil, class);
    expect_value(__wrap_rtnl_htb_set_ceil, ceil, 15625000); //Takes over the rate value.
    retval = activate_htb_queue(queue, parentid);
    assert_int_equal(retval, 0);

    //ShapingRate is set to 40 Mbps, AssuredRate is set too low (<8).
    will_return(__wrap_qos_node_queue_get_interface, "eth0");
    will_return(__wrap_qos_node_queue_get_index, 1);
    will_return(__wrap_qos_node_queue_get_shaping_rate, 40000000);
    will_return(__wrap_qos_node_queue_get_assured_rate, 5);
    will_return(__wrap_qos_node_queue_get_precedence, 1);
    will_return(__wrap_qos_node_queue_get_shaping_burst_size, 1500);
    expect_any(__wrap_rtnl_htb_set_rate, class);
    expect_value(__wrap_rtnl_htb_set_rate, rate, 5000000); //ShapingRate / 8.
    expect_any(__wrap_rtnl_htb_set_ceil, class);
    expect_value(__wrap_rtnl_htb_set_ceil, ceil, 5000000); //Takes over the rate value.
    retval = activate_htb_queue(queue, parentid);
    assert_int_equal(retval, 0);

    //ShapingRate is set to 40 Mbps, AssuredRate is set 15 Mbps.
    //AssuredRate becomes the HTB class rate, while the ShapingRate is the ceil.
    will_return(__wrap_qos_node_queue_get_interface, "eth0");
    will_return(__wrap_qos_node_queue_get_index, 1);
    will_return(__wrap_qos_node_queue_get_shaping_rate, 40000000);
    will_return(__wrap_qos_node_queue_get_assured_rate, 15000000);
    will_return(__wrap_qos_node_queue_get_precedence, 1);
    will_return(__wrap_qos_node_queue_get_shaping_burst_size, 1500);
    expect_any(__wrap_rtnl_htb_set_rate, class);
    expect_value(__wrap_rtnl_htb_set_rate, rate, 1875000); //AssuredRate / 8.
    expect_any(__wrap_rtnl_htb_set_ceil, class);
    expect_value(__wrap_rtnl_htb_set_ceil, ceil, 5000000); //ShapingRate / 8.
    retval = activate_htb_queue(queue, parentid);
    assert_int_equal(retval, 0);

    //ShapingRate equals AssuredRate.
    will_return(__wrap_qos_node_queue_get_interface, "eth0");
    will_return(__wrap_qos_node_queue_get_index, 1);
    will_return(__wrap_qos_node_queue_get_shaping_rate, 40000000);
    will_return(__wrap_qos_node_queue_get_assured_rate, 40000000);
    will_return(__wrap_qos_node_queue_get_precedence, 1);
    will_return(__wrap_qos_node_queue_get_shaping_burst_size, 1500);
    expect_any(__wrap_rtnl_htb_set_rate, class);
    expect_value(__wrap_rtnl_htb_set_rate, rate, 5000000); //AssuredRate / 8.
    expect_any(__wrap_rtnl_htb_set_ceil, class);
    expect_value(__wrap_rtnl_htb_set_ceil, ceil, 5000000); //ShapingRate / 8.
    retval = activate_htb_queue(queue, parentid);
    assert_int_equal(retval, 0);

    //AssuredRate is larger than ShapingRate.
    will_return(__wrap_qos_node_queue_get_interface, "eth0");
    will_return(__wrap_qos_node_queue_get_index, 1);
    will_return(__wrap_qos_node_queue_get_shaping_rate, 15000000);
    will_return(__wrap_qos_node_queue_get_assured_rate, 40000000);
    will_return(__wrap_qos_node_queue_get_precedence, 1);
    will_return(__wrap_qos_node_queue_get_shaping_burst_size, 1500);
    retval = activate_htb_queue(queue, parentid);
    assert_int_equal(retval, misconfigured);

    will_return(__wrap_qos_node_queue_get_interface, "eth0");
    will_return(__wrap_qos_node_queue_get_index, 1);
    will_return(__wrap_qos_node_queue_get_shaping_rate, 50000);
    will_return(__wrap_qos_node_queue_get_assured_rate, 50000);
    will_return(__wrap_qos_node_queue_get_precedence, 5);
    will_return(__wrap_qos_node_queue_get_shaping_burst_size, 3000);
    will_return(__wrap_qos_node_shaper_get_index, 1);
    expect_any(__wrap_rtnl_htb_set_rate, class);
    expect_any(__wrap_rtnl_htb_set_rate, rate);
    expect_any(__wrap_rtnl_htb_set_ceil, class);
    expect_any(__wrap_rtnl_htb_set_ceil, ceil);
    retval = activate_htb_queue(queue, no_parentid);
    assert_int_equal(retval, 0);

    will_return(__wrap_qos_node_queue_get_interface, NULL);
    will_return(__wrap_qos_node_queue_get_index, 0);
    will_return(__wrap_qos_node_queue_get_shaping_rate, 0);
    will_return(__wrap_qos_node_queue_get_assured_rate, 50000);
    will_return(__wrap_qos_node_queue_get_precedence, 0);
    will_return(__wrap_qos_node_queue_get_shaping_burst_size, 0);
    retval = activate_htb_queue(invalid_queue, parentid);
    assert_int_equal(retval, misconfigured);
}

void test_activate_prio_scheduler(UNUSED void** state) {
    int retval = -1;
    const char* ifname = "eth0";
    uint8_t priomap[] = { 0, 1, 2, 3 };
    int misconfigured = -2;
    int error = -1;

    will_return_always(__wrap_nl_socket_alloc, 0);
    will_return_always(__wrap_nl_connect, 0);
    will_return_always(__wrap_rtnl_link_get_by_name, 0);
    will_return_always(__wrap_rtnl_link_alloc_cache, 0);
    will_return_always(__wrap_rtnl_qdisc_alloc, 0);

    retval = activate_prio_scheduler(NULL, 3, NULL, 0);
    assert_int_equal(retval, misconfigured);

    retval = activate_prio_scheduler(ifname, 2, NULL, 0);
    assert_int_equal(retval, misconfigured);

    will_return(__wrap_rtnl_tc_set_kind, -1);
    retval = activate_prio_scheduler(ifname, 3, NULL, 0);
    assert_int_equal(retval, error);

    will_return(__wrap_rtnl_tc_set_kind, 0);
    will_return(__wrap_rtnl_qdisc_prio_set_priomap, -1);
    retval = activate_prio_scheduler(ifname, 4, priomap, sizeof(priomap));
    assert_int_equal(retval, misconfigured);
}

void test_configure_tbf_shaper(UNUSED void** state) {
    int retval = -1;
    const char* ifname = "eth0";
    uint32_t qid = 1;
    int misconfigured = -2;
    int error = -1;

    will_return_always(__wrap_nl_socket_alloc, 0);
    will_return_always(__wrap_nl_connect, 0);
    will_return_always(__wrap_rtnl_link_get_by_name, 0);
    will_return_always(__wrap_rtnl_link_alloc_cache, 0);
    will_return_always(__wrap_rtnl_qdisc_alloc, 0);
    will_return_always(__wrap_rtnl_qdisc_add, 0);

    retval = configure_tbf_shaper(qid, NULL, 1000000, 1000, 10000);
    assert_int_equal(retval, misconfigured);

    retval = configure_tbf_shaper(qid, ifname, 0, 1000, 10000);
    assert_int_equal(retval, misconfigured);

    retval = configure_tbf_shaper(qid, ifname, 1000000, 0, 10000);
    assert_int_equal(retval, misconfigured);

    retval = configure_tbf_shaper(qid, ifname, 9999, 1000, 10000);
    assert_int_equal(retval, misconfigured);

    will_return(__wrap_rtnl_tc_set_kind, -1);
    retval = configure_tbf_shaper(qid, ifname, 1000000, 1000, 10000);
    assert_int_equal(retval, error);

    will_return(__wrap_rtnl_tc_set_kind, 0);
    retval = configure_tbf_shaper(0, ifname, 1000000, 1000, 10000);
    assert_int_equal(retval, 0);

    will_return(__wrap_rtnl_tc_set_kind, 0);
    retval = configure_tbf_shaper(qid, ifname, 1000000, 1000, 10000);
    assert_int_equal(retval, 0);

    will_return(__wrap_rtnl_tc_set_kind, 0);
    retval = configure_tbf_shaper(qid + SHAPER_OFFSET, ifname, 1000000, 1000, 10000);
    assert_int_equal(retval, 0);

}

void test_cleanup_tbf_shaper(UNUSED void** state) {
    int retval = -1;
    uint32_t qid = 1;
    const char* ifname = "eth0";
    int misconfigured = -2;
    int error = -1;

    //set mocks for connect to netlink
    will_return_always(__wrap_nl_socket_alloc, 0);
    will_return_always(__wrap_nl_connect, 0);
    will_return_always(__wrap_rtnl_link_get_by_name, 0);
    will_return_always(__wrap_rtnl_link_alloc_cache, 0);
    will_return_always(__wrap_rtnl_qdisc_alloc, 0);

    retval = cleanup_tbf_shaper(qid, NULL);
    assert_int_equal(retval, misconfigured);

    will_return(__wrap_rtnl_qdisc_delete, -1);
    retval = cleanup_tbf_shaper(qid, ifname);
    assert_int_equal(retval, error);

    will_return(__wrap_rtnl_qdisc_delete, 0);
    retval = cleanup_tbf_shaper(0, ifname);
    assert_int_equal(retval, 0);

    will_return(__wrap_rtnl_qdisc_delete, 0);
    retval = cleanup_tbf_shaper(qid, ifname);
    assert_int_equal(retval, 0);

    will_return(__wrap_rtnl_qdisc_delete, 0);
    retval = cleanup_tbf_shaper(qid + SHAPER_OFFSET, ifname);
    assert_int_equal(retval, 0);
}

void test_configure_drop_algorithm(UNUSED void** state) {
    int retval = -1;
    uint32_t qid = 1;
    const char* ifname = "eth0";
    uint32_t limit = 0;
    qos_queue_drop_algorithm_t dropalgo_1 = QOS_QUEUE_DROP_ALGORITHM_RED;
    qos_queue_drop_algorithm_t dropalgo_2 = QOS_QUEUE_DROP_ALGORITHM_SFQ;
    int misconfigured = -2;

    //set mocks for connect to netlink
    will_return_always(__wrap_nl_socket_alloc, 0);
    will_return_always(__wrap_nl_connect, 0);
    will_return_always(__wrap_rtnl_link_get_by_name, 0);
    will_return_always(__wrap_rtnl_link_alloc_cache, 0);
    will_return(__wrap_rtnl_qdisc_alloc, -1);
    retval = configure_drop_algorithm(qid, ifname, dropalgo_1, limit);
    assert_int_equal(retval, -1);

    will_return_always(__wrap_rtnl_qdisc_alloc, 0);

    will_return(__wrap_rtnl_tc_set_kind, -1);
    retval = configure_drop_algorithm(qid, ifname, dropalgo_1, limit);
    assert_int_equal(retval, -1);

    will_return_always(__wrap_rtnl_tc_set_kind, 0);
    will_return(__wrap_rtnl_qdisc_add, -1);
    retval = configure_drop_algorithm(qid, ifname, dropalgo_1, limit);
    assert_int_equal(retval, -1);

    will_return_always(__wrap_rtnl_qdisc_add, 0);

    retval = configure_drop_algorithm(0, ifname, dropalgo_1, limit);
    assert_int_equal(retval, 0);

    retval = configure_drop_algorithm(qid, NULL, dropalgo_1, limit);
    assert_int_equal(retval, misconfigured);

    retval = configure_drop_algorithm(qid, ifname, QOS_QUEUE_DROP_ALGORITHM_LAST, limit);
    assert_int_equal(retval, misconfigured);

    retval = configure_drop_algorithm(qid, ifname, dropalgo_1, limit);
    assert_int_equal(retval, 0);

    retval = configure_drop_algorithm(SHAPER_OFFSET + qid, ifname, dropalgo_2, limit);
    assert_int_equal(retval, 0);

}

void test_cleanup_drop_algorithm(UNUSED void** state) {
    int retval = -1;
    uint32_t qid = 1;
    const char* ifname = "eth0";
    int misconfigured = -2;
    int error = -1;

    //set mocks for connect to netlink
    will_return_always(__wrap_nl_socket_alloc, 0);
    will_return_always(__wrap_nl_connect, 0);
    will_return_always(__wrap_rtnl_link_get_by_name, 0);
    will_return_always(__wrap_rtnl_link_alloc_cache, 0);
    will_return(__wrap_rtnl_qdisc_alloc, -1);

    retval = cleanup_drop_algorithm(qid, ifname);
    assert_int_equal(retval, error);
    will_return_always(__wrap_rtnl_qdisc_alloc, 0);
    will_return(__wrap_rtnl_qdisc_delete, -1);
    retval = cleanup_drop_algorithm(qid, ifname);
    assert_int_equal(retval, error);

    will_return_always(__wrap_rtnl_qdisc_delete, 0);
    retval = cleanup_drop_algorithm(qid, NULL);
    assert_int_equal(retval, misconfigured);

    retval = cleanup_drop_algorithm(0, ifname);
    assert_int_equal(retval, 0);

    retval = cleanup_drop_algorithm(qid, ifname);
    assert_int_equal(retval, 0);

    retval = cleanup_drop_algorithm(SHAPER_OFFSET + qid, ifname);
    assert_int_equal(retval, 0);
}

void test_qos_queue_activate(UNUSED void** state) {
    int retval = -1;
    amxc_var_t* args = NULL;
    amxc_var_t* no_shapingrate = NULL;
    amxc_var_t* ret = NULL;
    amxc_var_t* invalid = NULL;
    amxc_var_new(&ret);
    int misconfigured = -2;
    qos_node_t queue = {0};

    queue.type = QOS_NODE_TYPE_QUEUE;
    //set mocks for qos_queue_activate
    will_return_always(__wrap_qos_node_queue_get_interface, "eth0");
    will_return_always(__wrap_qos_node_queue_get_queue_key, 2051);
    will_return_always(__wrap_qos_node_queue_get_red_threshold, 0);
    will_return_always(__wrap_nl_socket_alloc, 0);
    will_return_always(__wrap_nl_connect, 0);
    will_return_always(__wrap_rtnl_link_get_by_name, 0);
    will_return_always(__wrap_rtnl_link_alloc_cache, 0);
    will_return_always(__wrap_rtnl_class_alloc, 0);
    will_return_always(__wrap_rtnl_qdisc_alloc, 0);
    will_return_always(__wrap_rtnl_htb_set_rate, 0);
    will_return_always(__wrap_rtnl_htb_set_ceil, 0);
    will_return_always(__wrap_rtnl_htb_set_prio, 0);
    will_return_always(__wrap_rtnl_htb_set_cbuffer, 0);
    will_return_always(__wrap_rtnl_class_add, 0);
    will_return_always(__wrap_rtnl_qdisc_add, 0);
    will_return_always(__wrap_qos_node_queue_get_precedence, 5);
    will_return_always(__wrap_qos_node_queue_get_shaping_burst_size, 3000);

    expect_any_always(__wrap_rtnl_htb_set_rate, class);
    expect_any_always(__wrap_rtnl_htb_set_rate, rate);
    expect_any_always(__wrap_rtnl_htb_set_ceil, class);
    expect_any_always(__wrap_rtnl_htb_set_ceil, ceil);

    //set mocks for configure filters
    will_return_always(__wrap_rtnl_cls_alloc, 0);
    will_return_always(__wrap_rtnl_tc_set_kind, 0);
    will_return_always(__wrap_rtnl_fw_set_classid, 0);
    will_return_always(__wrap_rtnl_fw_set_mask, 0);
    will_return_always(__wrap_qos_node_get_mark_mask, 31);

    will_return(__wrap_qos_node_get_node, &queue);
    will_return(__wrap_qos_node_find_parent_by_type, 1);
    will_return(__wrap_qos_node_queue_get_shaping_rate, 50000);
    will_return(__wrap_qos_node_queue_get_scheduler_algorithm, QOS_SCHEDULER_ALGORITHM_HTB);
    will_return(__wrap_qos_node_queue_get_drop_algorithm, QOS_QUEUE_DROP_ALGORITHM_LAST);
    will_return(__wrap_qos_node_queue_get_index, 1);
    will_return(__wrap_qos_node_queue_get_index, 4);
    will_return(__wrap_qos_node_queue_get_index, 1);
    will_return(__wrap_qos_node_queue_get_shaping_rate, 50000);
    will_return(__wrap_qos_node_queue_get_assured_rate, 50000);
    will_return(__wrap_qos_node_queue_get_index, 1);
    retval = qos_queue_activate("test", args, ret);
    assert_int_equal(retval, 0);

    will_return(__wrap_qos_node_get_node, NULL);
    will_return(__wrap_qos_node_find_parent_by_type, 0);
    //simulate no ifname or no qid
    retval = qos_queue_activate("test", invalid, ret);
    assert_int_equal(retval, misconfigured);

    will_return(__wrap_qos_node_get_node, &queue);
    will_return(__wrap_qos_node_find_parent_by_type, 1);
    will_return(__wrap_qos_node_queue_get_shaping_rate, 0);
    will_return(__wrap_qos_node_queue_get_scheduler_algorithm, QOS_SCHEDULER_ALGORITHM_HTB);
    will_return(__wrap_qos_node_queue_get_drop_algorithm, QOS_QUEUE_DROP_ALGORITHM_SFQ);
    will_return(__wrap_qos_node_queue_get_index, 1);
    will_return(__wrap_qos_node_queue_get_index, 4);
    retval = qos_queue_activate("test", no_shapingrate, ret);
    assert_int_equal(retval, misconfigured);

    // TBF test
    will_return(__wrap_qos_node_get_node, &queue);
    will_return(__wrap_qos_node_find_parent_by_type, 0);
    will_return(__wrap_qos_node_queue_get_shaping_rate, 50000);
    will_return(__wrap_qos_node_queue_get_scheduler_algorithm, QOS_SCHEDULER_ALGORITHM_TBF);
    will_return(__wrap_qos_node_queue_get_drop_algorithm, QOS_QUEUE_DROP_ALGORITHM_RED);
    will_return(__wrap_qos_node_queue_get_index, 1);
    will_return(__wrap_qos_node_queue_get_index, 4);
    retval = qos_queue_activate("test", args, ret);
    assert_int_equal(retval, 0);

    // SP test
    will_return(__wrap_qos_node_get_node, &queue);
    will_return(__wrap_qos_node_find_parent_by_type, 0);
    will_return(__wrap_qos_node_queue_get_shaping_rate, 1000000);
    will_return(__wrap_qos_node_queue_get_scheduler_algorithm, QOS_SCHEDULER_ALGORITHM_SP);
    will_return(__wrap_qos_node_queue_get_drop_algorithm, QOS_QUEUE_DROP_ALGORITHM_DT);
    will_return(__wrap_qos_node_queue_get_buffer_length, 2000000);
    will_return(__wrap_qos_node_queue_get_index, 1);
    will_return(__wrap_qos_node_queue_get_index, 4);
    retval = qos_queue_activate("test", args, ret);
    assert_int_equal(retval, 0);

    will_return(__wrap_qos_node_get_node, &queue);
    will_return(__wrap_qos_node_find_parent_by_type, 0);
    will_return(__wrap_qos_node_queue_get_shaping_rate, 0);
    will_return(__wrap_qos_node_queue_get_scheduler_algorithm, QOS_SCHEDULER_ALGORITHM_SP);
    will_return(__wrap_qos_node_queue_get_drop_algorithm, QOS_QUEUE_DROP_ALGORITHM_DT);
    will_return(__wrap_qos_node_queue_get_index, 1);
    will_return(__wrap_qos_node_queue_get_index, 4);
    retval = qos_queue_activate("test", args, ret);
    assert_int_equal(retval, 0);

    amxc_var_delete(&ret);
}

void test_qos_queue_deactivate(UNUSED void** state) {
    int retval = -1;
    amxc_var_t* args = NULL;
    amxc_var_t* no_parent = NULL;
    amxc_var_t* ret = NULL;
    qos_node_t queue = {0};

    queue.type = QOS_NODE_TYPE_QUEUE;
    amxc_var_new(&ret);

    will_return_always(__wrap_nl_socket_alloc, 0);
    will_return_always(__wrap_nl_connect, 0);
    will_return_always(__wrap_rtnl_link_get_by_name, 0);
    will_return_always(__wrap_rtnl_link_alloc_cache, 0);
    will_return_always(__wrap_rtnl_qdisc_alloc, 0);
    will_return_always(__wrap_rtnl_class_alloc, 0);
    will_return_always(__wrap_rtnl_cls_alloc, 0);
    will_return_always(__wrap_rtnl_tc_set_kind, 0);
    will_return_always(__wrap_rtnl_fw_set_classid, 0);
    will_return_always(__wrap_rtnl_qdisc_delete, 0);
    will_return_always(__wrap_qos_node_queue_get_interface, "eth0");
    will_return_always(__wrap_qos_node_queue_get_queue_key, 2051);
    will_return_always(__wrap_qos_node_queue_get_precedence, 5);

    will_return(__wrap_rtnl_class_delete, 0);
    will_return(__wrap_qos_node_get_node, &queue);
    will_return(__wrap_qos_node_find_parent_by_type, 0);
    will_return(__wrap_qos_node_find_parent_by_type, 0);
    will_return(__wrap_qos_node_find_parent_by_type, 0);
    will_return(__wrap_qos_node_find_parent_by_type, 0);
    will_return(__wrap_qos_node_queue_get_index, 1);
    will_return(__wrap_qos_node_queue_get_index, 1);
    will_return(__wrap_qos_node_shaper_get_index, 1);
    will_return(__wrap_qos_node_queue_get_shaping_rate, 10000);
    will_return(__wrap_qos_node_queue_get_scheduler_algorithm, QOS_SCHEDULER_ALGORITHM_HTB);
    will_return(__wrap_qos_node_queue_get_drop_algorithm, QOS_QUEUE_DROP_ALGORITHM_LAST);
    retval = qos_queue_deactivate("test", no_parent, ret);
    assert_int_equal(retval, 0);

    will_return(__wrap_rtnl_class_delete, 0);
    will_return(__wrap_qos_node_get_node, &queue);
    will_return(__wrap_qos_node_find_parent_by_type, 0);
    will_return(__wrap_qos_node_find_parent_by_type, 1);
    will_return(__wrap_qos_node_find_parent_by_type, 0);
    will_return(__wrap_qos_node_find_parent_by_type, 0);
    will_return(__wrap_qos_node_find_parent_by_type, 1);
    will_return(__wrap_qos_node_find_parent_by_type, 1);
    will_return(__wrap_qos_node_queue_get_index, 1);
    will_return(__wrap_qos_node_queue_get_index, 1);
    will_return(__wrap_qos_node_shaper_get_index, 1);
    will_return(__wrap_qos_node_queue_get_shaping_rate, 10000);
    will_return(__wrap_qos_node_queue_get_scheduler_algorithm, QOS_SCHEDULER_ALGORITHM_HTB);
    will_return(__wrap_qos_node_queue_get_drop_algorithm, QOS_QUEUE_DROP_ALGORITHM_LAST);
    retval = qos_queue_deactivate("test", no_parent, ret);
    assert_int_equal(retval, 0);

    will_return(__wrap_rtnl_class_delete, 0);
    will_return(__wrap_qos_node_get_node, &queue);
    will_return(__wrap_qos_node_find_parent_by_type, 1);
    will_return(__wrap_qos_node_queue_get_index, 1);
    will_return(__wrap_qos_node_queue_get_index, 4);
    will_return(__wrap_qos_node_queue_get_index, 1);
    will_return(__wrap_qos_node_queue_get_shaping_rate, 10000);
    will_return(__wrap_qos_node_queue_get_scheduler_algorithm, QOS_SCHEDULER_ALGORITHM_HTB);
    will_return(__wrap_qos_node_queue_get_drop_algorithm, QOS_QUEUE_DROP_ALGORITHM_SFQ);
    retval = qos_queue_deactivate("test", args, ret);
    assert_int_equal(retval, 0);

    will_return(__wrap_qos_node_get_node, &queue);
    will_return(__wrap_qos_node_find_parent_by_type, 0);
    will_return(__wrap_qos_node_queue_get_index, 1);
    will_return(__wrap_qos_node_queue_get_index, 4);
    will_return(__wrap_qos_node_queue_get_index, 1);
    will_return(__wrap_qos_node_queue_get_shaping_rate, 10000);
    will_return(__wrap_qos_node_queue_get_scheduler_algorithm, QOS_SCHEDULER_ALGORITHM_TBF);
    will_return(__wrap_qos_node_queue_get_drop_algorithm, QOS_QUEUE_DROP_ALGORITHM_RED);
    retval = qos_queue_deactivate("test", args, ret);
    assert_int_equal(retval, 0);

    will_return(__wrap_qos_node_get_node, &queue);
    will_return(__wrap_qos_node_find_parent_by_type, 0);
    will_return(__wrap_qos_node_queue_get_index, 1);
    will_return(__wrap_qos_node_queue_get_shaping_rate, 10000);
    will_return(__wrap_qos_node_queue_get_scheduler_algorithm, QOS_SCHEDULER_ALGORITHM_SP);
    will_return(__wrap_qos_node_queue_get_drop_algorithm, QOS_QUEUE_DROP_ALGORITHM_SFQ);
    retval = qos_queue_deactivate("test", args, ret);
    assert_int_equal(retval, 0);

    amxc_var_delete(&no_parent);
    amxc_var_delete(&ret);
}

void test_qos_shaper_activate(UNUSED void** state) {
    int retval = -1;
    amxc_var_t* args = NULL;
    amxc_var_t* args2 = NULL;
    amxc_var_t* args_misconfigured = NULL;
    amxc_var_t* ret = NULL;
    amxc_var_new(&ret);
    int misconfigured = -2;
    int error = -1;

    will_return_always(__wrap_nl_socket_alloc, 0);
    will_return_always(__wrap_nl_connect, 0);
    will_return_always(__wrap_rtnl_link_get_by_name, 0);
    will_return_always(__wrap_rtnl_link_alloc_cache, 0);
    will_return_always(__wrap_rtnl_class_alloc, 0);
    will_return_always(__wrap_rtnl_tc_set_kind, 0);
    will_return_always(__wrap_rtnl_htb_set_rate, 0);
    will_return_always(__wrap_rtnl_htb_set_cbuffer, 0);

    expect_any_always(__wrap_rtnl_htb_set_rate, class);
    expect_any_always(__wrap_rtnl_htb_set_rate, rate);

    will_return(__wrap_rtnl_class_add, -1);
    will_return(__wrap_qos_node_get_node, 1);
    will_return(__wrap_qos_node_find_parent_by_type, 1);
    will_return(__wrap_qos_node_scheduler_get_scheduler_algorithm, QOS_SCHEDULER_ALGORITHM_HTB);
    will_return(__wrap_qos_node_shaper_get_interface, "eth0");
    will_return(__wrap_qos_node_shaper_get_shaping_rate, 500000);
    will_return(__wrap_qos_node_shaper_get_shaping_burst_size, 30000);
    will_return(__wrap_qos_node_shaper_get_index, 1);
    retval = qos_shaper_activate("test", args2, ret);
    assert_int_equal(retval, error);

    will_return_always(__wrap_rtnl_class_add, 0);

    will_return(__wrap_qos_node_get_node, 1);
    will_return(__wrap_qos_node_find_parent_by_type, 1);
    will_return(__wrap_qos_node_scheduler_get_scheduler_algorithm, QOS_SCHEDULER_ALGORITHM_HTB);
    will_return(__wrap_qos_node_shaper_get_interface, "eth0");
    will_return(__wrap_qos_node_shaper_get_shaping_rate, 500000);
    will_return(__wrap_qos_node_shaper_get_shaping_burst_size, 30000);
    will_return(__wrap_qos_node_shaper_get_index, 1);
    retval = qos_shaper_activate("test", args, ret);
    assert_int_equal(retval, 0);

    will_return(__wrap_qos_node_get_node, 0);
    will_return(__wrap_qos_node_find_parent_by_type, 1);
    retval = qos_shaper_activate("test", args_misconfigured, ret);
    assert_int_equal(retval, misconfigured);

    will_return(__wrap_qos_node_get_node, 1);
    will_return(__wrap_qos_node_find_parent_by_type, 0);
    will_return(__wrap_qos_node_find_child_by_type, 0);
    retval = qos_shaper_activate("test", args_misconfigured, ret);
    assert_int_equal(retval, misconfigured);

    will_return(__wrap_qos_node_get_node, 1);
    will_return(__wrap_qos_node_find_parent_by_type, 1);
    will_return(__wrap_qos_node_scheduler_get_scheduler_algorithm, QOS_SCHEDULER_ALGORITHM_TBF);
    retval = qos_shaper_activate("test", args, ret);
    assert_int_equal(retval, 0);

    will_return(__wrap_qos_node_get_node, 1);
    will_return(__wrap_qos_node_find_parent_by_type, 1);
    will_return(__wrap_qos_node_scheduler_get_scheduler_algorithm, QOS_SCHEDULER_ALGORITHM_SP);
    retval = qos_shaper_activate("test", args, ret);
    assert_int_equal(retval, 0);

    will_return(__wrap_qos_node_get_node, 1);
    will_return(__wrap_qos_node_find_parent_by_type, 1);
    will_return(__wrap_qos_node_scheduler_get_scheduler_algorithm, QOS_SCHEDULER_ALGORITHM_LAST);
    retval = qos_shaper_activate("test", args, ret);
    assert_int_equal(retval, misconfigured);

    amxc_var_delete(&ret);
}

void test_qos_shaper_deactivate(UNUSED void** state) {
    int retval = -1;
    amxc_var_t* args_invalid = NULL;
    amxc_var_t* args = NULL;
    amxc_var_t* args2 = NULL;
    amxc_var_t* ret = NULL;
    amxc_var_new(&ret);
    int misconfigured = -2;
    int error = -1;

    will_return_always(__wrap_nl_socket_alloc, 0);
    will_return_always(__wrap_nl_connect, 0);
    will_return_always(__wrap_rtnl_link_get_by_name, 0);
    will_return_always(__wrap_rtnl_link_alloc_cache, 0);
    will_return_always(__wrap_rtnl_class_alloc, 0);

    will_return(__wrap_rtnl_class_delete, -1);
    will_return(__wrap_qos_node_get_node, 1);
    will_return(__wrap_qos_node_shaper_get_interface, "eth0");
    will_return(__wrap_qos_node_shaper_get_index, 1);
    will_return(__wrap_qos_node_find_parent_by_type, 1);
    will_return(__wrap_qos_node_scheduler_get_scheduler_algorithm, QOS_SCHEDULER_ALGORITHM_HTB);
    retval = qos_shaper_deactivate("test", args2, ret);
    assert_int_equal(retval, error);

    will_return_always(__wrap_rtnl_class_delete, 0);

    will_return(__wrap_qos_node_get_node, 1);
    will_return(__wrap_qos_node_shaper_get_interface, "eth0");
    will_return(__wrap_qos_node_shaper_get_index, 1);
    will_return(__wrap_qos_node_find_parent_by_type, 1);
    will_return(__wrap_qos_node_scheduler_get_scheduler_algorithm, QOS_SCHEDULER_ALGORITHM_HTB);
    retval = qos_shaper_deactivate("test", args, ret);
    assert_int_equal(retval, 0);

    will_return(__wrap_qos_node_get_node, 1);
    will_return(__wrap_qos_node_shaper_get_interface, "eth0");
    will_return(__wrap_qos_node_find_parent_by_type, 1);
    will_return(__wrap_qos_node_scheduler_get_scheduler_algorithm, QOS_SCHEDULER_ALGORITHM_TBF);
    retval = qos_shaper_deactivate("test", args, ret);
    assert_int_equal(retval, 0);

    will_return(__wrap_qos_node_get_node, 1);
    will_return(__wrap_qos_node_shaper_get_interface, "eth0");
    will_return(__wrap_qos_node_find_parent_by_type, 1);
    will_return(__wrap_qos_node_scheduler_get_scheduler_algorithm, QOS_SCHEDULER_ALGORITHM_SP);
    retval = qos_shaper_deactivate("test", args, ret);
    assert_int_equal(retval, 0);

    will_return(__wrap_qos_node_get_node, NULL);
    will_return(__wrap_qos_node_shaper_get_interface, "eth0");
    will_return(__wrap_qos_node_find_parent_by_type, 1);
    retval = qos_shaper_deactivate("test", args_invalid, ret);
    assert_int_equal(retval, misconfigured);

    will_return(__wrap_qos_node_get_node, 1);
    will_return(__wrap_qos_node_shaper_get_interface, NULL);
    will_return(__wrap_qos_node_find_parent_by_type, 1);
    retval = qos_shaper_deactivate("test", args_invalid, ret);
    assert_int_equal(retval, misconfigured);

    will_return(__wrap_qos_node_get_node, 1);
    will_return(__wrap_qos_node_find_parent_by_type, 0);
    will_return(__wrap_qos_node_find_child_by_type, 0);
    will_return(__wrap_qos_node_shaper_get_interface, "eth0");
    retval = qos_shaper_deactivate("test", args_invalid, ret);
    assert_int_equal(retval, misconfigured);

    amxc_var_delete(&ret);
}

void test_qos_scheduler_activate(UNUSED void** state) {
    int retval = -1;
    amxc_var_t* args_invalid = NULL;
    amxc_var_t* args = NULL;
    amxc_var_t* args2 = NULL;
    amxc_var_t* ret = NULL;
    amxc_var_new(&ret);
    int error = -1;
    int misconfigured = -2;

    will_return_always(__wrap_nl_socket_alloc, 0);
    will_return_always(__wrap_nl_connect, 0);
    will_return_always(__wrap_rtnl_link_get_by_name, 0);
    will_return_always(__wrap_rtnl_link_alloc_cache, 0);
    will_return_always(__wrap_rtnl_qdisc_alloc, 0);
    will_return_always(__wrap_rtnl_tc_set_kind, 0);

    will_return(__wrap_rtnl_qdisc_add, -1);
    will_return(__wrap_qos_node_get_node, 1);
    will_return(__wrap_qos_node_scheduler_get_interface, "eth0");
    will_return(__wrap_qos_node_scheduler_get_interface, "eth0");
    will_return(__wrap_qos_node_scheduler_get_default_queue_node, 0);
    will_return(__wrap_qos_node_scheduler_get_scheduler_algorithm, QOS_SCHEDULER_ALGORITHM_HTB);
    will_return(__wrap_qos_node_queue_get_index, 0);
    retval = qos_scheduler_activate("test", args2, ret);
    assert_int_equal(retval, error);

    will_return_always(__wrap_rtnl_qdisc_add, 0);

    will_return(__wrap_qos_node_get_node, 1);
    will_return(__wrap_qos_node_scheduler_get_interface, "eth0");
    will_return(__wrap_qos_node_scheduler_get_interface, "eth0");
    will_return(__wrap_qos_node_scheduler_get_default_queue_node, 0);
    will_return(__wrap_qos_node_scheduler_get_scheduler_algorithm, QOS_SCHEDULER_ALGORITHM_HTB);
    will_return(__wrap_qos_node_queue_get_index, 0);
    retval = qos_scheduler_activate("test", args, ret);
    assert_int_equal(retval, 0);

    will_return(__wrap_qos_node_get_node, 0);
    will_return(__wrap_qos_node_scheduler_get_interface, "eth0");
    will_return(__wrap_qos_node_scheduler_get_default_queue_node, 0);
    will_return(__wrap_qos_node_scheduler_get_scheduler_algorithm, QOS_SCHEDULER_ALGORITHM_HTB);
    retval = qos_scheduler_activate("test", args_invalid, ret);
    assert_int_equal(retval, misconfigured);

    will_return(__wrap_qos_node_get_node, 1);
    will_return(__wrap_qos_node_scheduler_get_interface, NULL);
    will_return(__wrap_qos_node_scheduler_get_default_queue_node, 0);
    will_return(__wrap_qos_node_scheduler_get_scheduler_algorithm, QOS_SCHEDULER_ALGORITHM_HTB);
    retval = qos_scheduler_activate("test", args_invalid, ret);
    assert_int_equal(retval, misconfigured);

    // TBF tests
    will_return(__wrap_qos_node_get_node, 1);
    will_return(__wrap_qos_node_scheduler_get_interface, "eth0");
    will_return(__wrap_qos_node_scheduler_get_default_queue_node, 0);
    will_return(__wrap_qos_node_scheduler_get_scheduler_algorithm, QOS_SCHEDULER_ALGORITHM_TBF);
    will_return(__wrap_qos_node_find_child_by_type, 1);
    will_return(__wrap_qos_node_get_children, 1);
    will_return(__wrap_amxc_llist_size, 1);
    will_return(__wrap_qos_node_queue_get_shaping_rate, 1000000);
    will_return(__wrap_qos_node_queue_get_buffer_length, 2000000);
    will_return(__wrap_qos_node_queue_get_shaping_burst_size, 10000);
    retval = qos_scheduler_activate("test", args, ret);
    assert_int_equal(retval, 0);

    will_return(__wrap_qos_node_get_node, 1);
    will_return(__wrap_qos_node_scheduler_get_interface, "eth0");
    will_return(__wrap_qos_node_scheduler_get_default_queue_node, 0);
    will_return(__wrap_qos_node_scheduler_get_scheduler_algorithm, QOS_SCHEDULER_ALGORITHM_TBF);
    will_return(__wrap_qos_node_find_child_by_type, 0);
    will_return(__wrap_qos_node_find_child_by_type, 1);
    will_return(__wrap_qos_node_find_child_by_type, 1);
    will_return(__wrap_qos_node_get_children, 1);
    will_return(__wrap_amxc_llist_size, 1);
    will_return(__wrap_qos_node_queue_get_shaping_rate, 1000000);
    will_return(__wrap_qos_node_queue_get_buffer_length, 2000000);
    will_return(__wrap_qos_node_queue_get_shaping_burst_size, 10000);
    retval = qos_scheduler_activate("test", args, ret);
    assert_int_equal(retval, 0);

    will_return(__wrap_qos_node_get_node, 1);
    will_return(__wrap_qos_node_scheduler_get_interface, "eth0");
    will_return(__wrap_qos_node_scheduler_get_default_queue_node, 0);
    will_return(__wrap_qos_node_scheduler_get_scheduler_algorithm, QOS_SCHEDULER_ALGORITHM_TBF);
    will_return(__wrap_qos_node_find_child_by_type, 0);
    will_return(__wrap_qos_node_find_child_by_type, 1);
    will_return(__wrap_qos_node_find_child_by_type, 1);
    will_return(__wrap_qos_node_get_children, 1);
    will_return(__wrap_amxc_llist_size, 1);
    will_return(__wrap_qos_node_queue_get_shaping_rate, 0);
    retval = qos_scheduler_activate("test", args, ret);
    assert_int_equal(retval, misconfigured);

    will_return(__wrap_qos_node_get_node, 1);
    will_return(__wrap_qos_node_scheduler_get_interface, "eth0");
    will_return(__wrap_qos_node_scheduler_get_default_queue_node, 0);
    will_return(__wrap_qos_node_scheduler_get_scheduler_algorithm, QOS_SCHEDULER_ALGORITHM_TBF);
    will_return(__wrap_qos_node_find_child_by_type, 0);
    will_return(__wrap_qos_node_find_child_by_type, 1);
    will_return(__wrap_qos_node_find_child_by_type, 1);
    will_return(__wrap_qos_node_get_children, 1);
    will_return(__wrap_amxc_llist_size, 2);
    retval = qos_scheduler_activate("test", args, ret);
    assert_int_equal(retval, misconfigured);

    will_return(__wrap_qos_node_get_node, 1);
    will_return(__wrap_qos_node_scheduler_get_interface, "eth0");
    will_return(__wrap_qos_node_scheduler_get_default_queue_node, 0);
    will_return(__wrap_qos_node_scheduler_get_scheduler_algorithm, QOS_SCHEDULER_ALGORITHM_TBF);
    will_return(__wrap_qos_node_find_child_by_type, 0);
    will_return(__wrap_qos_node_find_child_by_type, 0);
    retval = qos_scheduler_activate("test", args, ret);
    assert_int_equal(retval, misconfigured);

    // SP tests
    will_return(__wrap_qos_node_get_node, 1);
    will_return(__wrap_qos_node_scheduler_get_interface, "eth0");
    will_return(__wrap_qos_node_scheduler_get_default_queue_node, 0);
    will_return(__wrap_qos_node_scheduler_get_scheduler_algorithm, QOS_SCHEDULER_ALGORITHM_SP);
    will_return(__wrap_qos_node_find_child_by_type, 1);
    will_return(__wrap_qos_node_get_children, 1);
    will_return(__wrap_amxc_llist_size, 4);
    will_return(__wrap_qos_node_get_broken_qdisc_priomap, true);
    will_return(__wrap_rtnl_qdisc_prio_set_priomap, 0);
    retval = qos_scheduler_activate("test", args, ret);
    assert_int_equal(retval, 0);

    will_return(__wrap_qos_node_get_node, 1);
    will_return(__wrap_qos_node_scheduler_get_interface, "eth0");
    will_return(__wrap_qos_node_scheduler_get_default_queue_node, 0);
    will_return(__wrap_qos_node_scheduler_get_scheduler_algorithm, QOS_SCHEDULER_ALGORITHM_SP);
    will_return(__wrap_qos_node_find_child_by_type, 0);
    will_return(__wrap_qos_node_find_child_by_type, 1);
    will_return(__wrap_qos_node_get_children, 1);
    will_return(__wrap_amxc_llist_size, 6);
    will_return(__wrap_qos_node_get_broken_qdisc_priomap, false);
    will_return(__wrap_rtnl_qdisc_prio_set_priomap, 0);
    retval = qos_scheduler_activate("test", args, ret);
    assert_int_equal(retval, 0);

    will_return(__wrap_qos_node_get_node, 1);
    will_return(__wrap_qos_node_scheduler_get_interface, "eth0");
    will_return(__wrap_qos_node_scheduler_get_default_queue_node, 0);
    will_return(__wrap_qos_node_scheduler_get_scheduler_algorithm, QOS_SCHEDULER_ALGORITHM_SP);
    will_return(__wrap_qos_node_find_child_by_type, 0);
    will_return(__wrap_qos_node_find_child_by_type, 0);
    retval = qos_scheduler_activate("test", args_invalid, ret);
    assert_int_equal(retval, misconfigured);

    will_return(__wrap_qos_node_get_node, 1);
    will_return(__wrap_qos_node_scheduler_get_interface, "eth0");
    will_return(__wrap_qos_node_scheduler_get_default_queue_node, 0);
    will_return(__wrap_qos_node_scheduler_get_scheduler_algorithm, QOS_SCHEDULER_ALGORITHM_SP);
    will_return(__wrap_qos_node_find_child_by_type, 0);
    will_return(__wrap_qos_node_find_child_by_type, 0);
    retval = qos_scheduler_activate("test", args_invalid, ret);
    assert_int_equal(retval, misconfigured);

    // Invalid scheduler algorithm
    will_return(__wrap_qos_node_get_node, 1);
    will_return(__wrap_qos_node_scheduler_get_interface, "eth0");
    will_return(__wrap_qos_node_scheduler_get_default_queue_node, 0);
    will_return(__wrap_qos_node_scheduler_get_scheduler_algorithm, QOS_SCHEDULER_ALGORITHM_LAST);
    retval = qos_scheduler_activate("test", args_invalid, ret);
    assert_int_equal(retval, misconfigured);

    amxc_var_delete(&ret);
}

void test_qos_scheduler_deactivate(UNUSED void** state) {
    int retval = -1;
    amxc_var_t* args_invalid = NULL;
    amxc_var_t* args = NULL;
    amxc_var_t* args2 = NULL;
    amxc_var_t* ret = NULL;
    amxc_var_new(&ret);
    int error = -1;
    int misconfigured = -2;

    will_return_always(__wrap_nl_socket_alloc, 0);
    will_return_always(__wrap_nl_connect, 0);
    will_return_always(__wrap_rtnl_link_get_by_name, 0);
    will_return_always(__wrap_rtnl_link_alloc_cache, 0);
    will_return_always(__wrap_rtnl_qdisc_alloc, 0);

    will_return(__wrap_rtnl_qdisc_delete, -1);
    will_return(__wrap_qos_node_get_node, 0);
    will_return(__wrap_qos_node_scheduler_get_interface, "eth0");
    retval = qos_scheduler_deactivate("test", args2, ret);
    assert_int_equal(retval, error);

    will_return_always(__wrap_rtnl_qdisc_delete, 0);

    will_return(__wrap_qos_node_get_node, 0);
    will_return(__wrap_qos_node_scheduler_get_interface, "eth0");
    retval = qos_scheduler_deactivate("test", args, ret);
    assert_int_equal(retval, 0);

    will_return(__wrap_qos_node_get_node, 0);
    will_return(__wrap_qos_node_scheduler_get_interface, NULL);
    retval = qos_scheduler_deactivate("test", args_invalid, ret);
    assert_int_equal(retval, misconfigured);

    amxc_var_delete(&ret);
}

void test_qos_stats_retrieve(UNUSED void** state) {
    int retval = -1;
    amxc_var_t* args = NULL;
    amxc_var_t* args2 = NULL;
    amxc_var_t* args_invalid = NULL;
    amxc_var_t* ret = NULL;
    amxc_var_new(&ret);

    will_return_always(__wrap_nl_socket_alloc, 0);
    will_return_always(__wrap_nl_connect, 0);
    will_return_always(__wrap_rtnl_link_get_by_name, 0);
    will_return_always(__wrap_rtnl_link_alloc_cache, 0);
    will_return(__wrap_rtnl_class_alloc, -1);

    will_return(__wrap_qos_node_get_node, 0);
    will_return(__wrap_qos_node_queue_get_interface, "eth0");
    will_return(__wrap_qos_node_queue_get_index, 1);
    retval = qos_stats_retrieve("test", args, ret);
    assert_int_equal(retval, -1);

    will_return_always(__wrap_rtnl_link_get_ifindex, 1);
    will_return_always(__wrap_rtnl_class_alloc_cache, 0);
    will_return_always(__wrap_rtnl_class_get, 0);

    will_return_always(__wrap_rtnl_class_alloc, 0);

    will_return(__wrap_rtnl_tc_get_stat, 246);
    will_return(__wrap_rtnl_tc_get_stat, 40);
    will_return(__wrap_rtnl_tc_get_stat, 30);

    will_return(__wrap_qos_node_get_node, 0);
    will_return(__wrap_qos_node_queue_get_interface, "eth0");
    will_return(__wrap_qos_node_queue_get_index, 1);
    retval = qos_stats_retrieve("test", args2, ret);
    assert_int_equal(retval, 0);

    assert_int_equal(GETP_UINT32(ret, "TxPackets"), 246);
    assert_int_equal(GETP_UINT32(ret, "TxBytes"), 40);
    assert_int_equal(GETP_UINT32(ret, "DroppedPackets"), 30);

    will_return(__wrap_qos_node_get_node, 0);
    will_return(__wrap_qos_node_queue_get_interface, "");
    will_return(__wrap_qos_node_queue_get_index, 0);
    retval = qos_stats_retrieve("test", args_invalid, ret);
    assert_int_equal(retval, -2);

    amxc_var_delete(&ret);
}

void test_qos_queue_stats_retrieve(UNUSED void** state) {
    int retval = -1;
    amxc_var_t* args = NULL;
    amxc_var_t* args2 = NULL;
    amxc_var_t* args_invalid = NULL;
    amxc_var_t* ret = NULL;
    amxc_var_new(&ret);

    will_return_always(__wrap_nl_socket_alloc, 0);
    will_return_always(__wrap_nl_connect, 0);
    will_return_always(__wrap_rtnl_link_get_by_name, 0);
    will_return_always(__wrap_rtnl_link_alloc_cache, 0);
    will_return(__wrap_rtnl_class_alloc, -1);

    will_return(__wrap_qos_node_get_node, 0);
    will_return(__wrap_qos_node_queue_get_interface, "eth0");
    will_return(__wrap_qos_node_queue_get_index, 1);
    retval = qos_queue_stats_retrieve("test", args, ret);
    assert_int_equal(retval, -1);

    will_return_always(__wrap_rtnl_link_get_ifindex, 1);
    will_return_always(__wrap_rtnl_class_alloc_cache, 0);
    will_return_always(__wrap_rtnl_class_get, 0);

    will_return_always(__wrap_rtnl_class_alloc, 0);

    will_return(__wrap_rtnl_tc_get_stat, 50000);
    will_return(__wrap_qos_node_queue_get_assured_rate, 50000);

    will_return(__wrap_qos_node_get_node, 0);
    will_return(__wrap_qos_node_queue_get_interface, "eth0");
    will_return(__wrap_qos_node_queue_get_index, 1);
    retval = qos_queue_stats_retrieve("test", args2, ret);
    assert_int_equal(retval, 0);

    assert_int_equal(GETP_UINT32(ret, "CurrentShapingRate"), 50000);
    assert_int_equal(GETP_UINT32(ret, "CurrentAssuredRate"), 50000);

    will_return(__wrap_qos_node_get_node, 0);
    will_return(__wrap_qos_node_queue_get_interface, "");
    will_return(__wrap_qos_node_queue_get_index, 0);
    retval = qos_stats_retrieve("test", args_invalid, ret);
    assert_int_equal(retval, -2);

    amxc_var_delete(&ret);
}

void test_mod_tc_start(UNUSED void** state) {
    int retval = -1;

    expect_any_always(__wrap_qos_module_register, module);
    expect_any_always(__wrap_qos_module_register, funcs);

    will_return(__wrap_qos_module_register, -1);
    retval = mod_tc_start();
    assert_int_equal(retval, -1);

    will_return(__wrap_qos_module_register, 0);
    retval = mod_tc_start();
    assert_int_equal(retval, 0);
}

void test_mod_tc_stop(UNUSED void** state) {
    int retval = -1;

    expect_any_always(__wrap_qos_module_deregister, module);
    will_return(__wrap_qos_module_deregister, 0);
    retval = mod_tc_stop();
    assert_int_equal(retval, 0);
}
