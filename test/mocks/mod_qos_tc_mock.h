/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef __TEST_MOD_QOD_TC_MOCK_H__
#define __TEST_MOD_QOS_TC_MOCK_H__

#include <netlink/route/tc.h>
#include <netlink/route/classifier.h>
#include <netlink/route/class.h>
#include <netlink/route/qdisc.h>
#include <netlink/route/qdisc/htb.h>
#include <amxc/amxc.h>
#include <qosnode/qos-node-api.h>

size_t __wrap_amxc_llist_size(amxc_llist_t*);
struct nl_sock* __wrap_nl_socket_alloc(void);
void __wrap_rtnl_tc_set_handle(struct rtnl_tc*, uint32_t);
void __wrap_rtnl_tc_set_link(struct rtnl_tc*, struct rtnl_link*);
void __wrap_rtnl_tc_set_parent(struct rtnl_tc*, uint32_t);
void __wrap_rtnl_cls_set_protocol(struct rtnl_cls*, uint16_t);
void __wrap_rtnl_cls_set_prio(struct rtnl_cls*, uint16_t);
int __wrap_rtnl_fw_set_classid(struct rtnl_cls*, uint32_t);
int __wrap_rtnl_fw_set_mask(struct rtnl_cls*, uint32_t);
int __wrap_rtnl_cls_add(struct nl_sock*, struct rtnl_cls*, int);
int __wrap_rtnl_tc_set_kind(struct rtnl_tc*, const char*);
int __wrap_rtnl_cls_delete(struct nl_sock*, struct rtnl_cls*, int);
void __wrap_rtnl_class_put(struct rtnl_class*);
void __wrap_rtnl_cls_put(struct rtnl_cls*);
void __wrap_nl_cache_put(struct nl_cache* cache);
void __wrap_rtnl_link_put(struct rtnl_link*);
void __wrap_nl_socket_free(struct nl_sock*);
int __wrap_nl_connect(struct nl_sock*, int);
int __wrap_rtnl_link_alloc_cache(struct nl_sock*, int, struct nl_cache**);
struct rtnl_link* __wrap_rtnl_link_get_by_name(struct nl_cache*, const char*);
struct rtnl_qdisc* __wrap_rtnl_qdisc_alloc(void);
struct rtnl_class* __wrap_rtnl_class_alloc(void);
struct rtnl_cls* __wrap_rtnl_cls_alloc(void);
void __wrap_rtnl_qdisc_put(struct rtnl_qdisc*);
int __wrap_rtnl_class_delete(struct nl_sock*, struct rtnl_class*);
int __wrap_rtnl_qdisc_delete(struct nl_sock*, struct rtnl_qdisc*);
int __wrap_rtnl_htb_set_defcls(struct rtnl_qdisc*, uint32_t);
int __wrap_rtnl_qdisc_add(struct nl_sock*, struct rtnl_qdisc*, int);
int __wrap_rtnl_htb_set_rate(struct rtnl_class*, uint32_t);
int __wrap_rtnl_htb_set_ceil(struct rtnl_class*, uint32_t);
int __wrap_rtnl_htb_set_cbuffer(struct rtnl_class*, uint32_t);
int __wrap_rtnl_class_add(struct nl_sock*, struct rtnl_class*, int);
int __wrap_rtnl_htb_set_prio(struct rtnl_class*, uint32_t);
void __wrap_rtnl_sfq_set_perturb(struct rtnl_qdisc*, uint32_t);
void __wrap_rtnl_red_set_limit (struct rtnl_qdisc*, int);
uint64_t __wrap_rtnl_tc_get_stat(struct rtnl_class*, enum rtnl_tc_stat);
int __wrap_rtnl_class_alloc_cache(struct nl_sock*, int, struct nl_cache*);
struct rtnl_class* __wrap_rtnl_class_get(struct nl_cache*, int, uint32_t);
int __wrap_rtnl_link_get_ifindex(struct rtnl_link*);
extern void __wrap_rtnl_qdisc_tbf_set_limit(struct rtnl_qdisc*, int);
extern void __wrap_rtnl_qdisc_tbf_set_rate(struct rtnl_qdisc*, int rate, int, int);
extern void __wrap_rtnl_qdisc_prio_set_bands(struct rtnl_qdisc*, int);
extern int __wrap_rtnl_qdisc_prio_set_priomap(struct rtnl_qdisc*, uint8_t[], int);
const char* __wrap_qos_node_queue_get_interface(qos_node_t*);
uint32_t __wrap_qos_node_queue_get_index(qos_node_t*);
uint32_t __wrap_qos_node_queue_get_queue_key(qos_node_t*);
uint32_t __wrap_qos_node_queue_get_precedence(qos_node_t*);
const char* __wrap_qos_node_scheduler_get_interface(qos_node_t*);
const char* __wrap_qos_node_shaper_get_interface(qos_node_t*);
uint32_t __wrap_qos_node_shaper_get_shaping_rate(qos_node_t*);
uint32_t __wrap_qos_node_shaper_get_shaping_burst_size(qos_node_t*);
uint32_t __wrap_qos_node_shaper_get_index(qos_node_t*);
int32_t __wrap_qos_node_queue_get_shaping_rate(qos_node_t*);
int32_t __wrap_qos_node_queue_get_assured_rate(qos_node_t*);
uint32_t __wrap_qos_node_queue_get_shaping_burst_size(qos_node_t*);
uint32_t __wrap_qos_node_queue_get_buffer_length(qos_node_t*);
qos_scheduler_algorithm_t __wrap_qos_node_queue_get_scheduler_algorithm(qos_node_t*);
uint32_t __wrap_qos_node_queue_get_red_threshold(qos_node_t*);
qos_queue_drop_algorithm_t __wrap_qos_node_queue_get_drop_algorithm(qos_node_t*);
qos_scheduler_algorithm_t __wrap_qos_node_scheduler_get_scheduler_algorithm(qos_node_t*);
qos_node_t* __wrap_qos_node_scheduler_get_default_queue_node(qos_node_t*);
qos_node_t* __wrap_qos_node_find_parent_by_type(const qos_node_t*, const qos_node_type_t);
qos_node_t* __wrap_qos_node_find_child_by_type(const qos_node_t*, const qos_node_type_t);
qos_node_t* __wrap_qos_node_get_node(const char*);
amxc_llist_t* __wrap_qos_node_get_children(const qos_node_t*);
uint32_t __wrap_qos_node_get_mark_mask(void);
bool __wrap_qos_node_get_broken_qdisc_priomap(void);

#endif //TEST_MOD_QOS_TC_MOCK_H
